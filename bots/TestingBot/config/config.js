const config = {

    // ROLES
    FACTION_RECRUIT_ROLE: "Recruit",
    FACTION_1_ROLE: "Operative United Member",
    FACTION_2_ROLE: "Vector Accelerate Member",
    FACTION_3_ROLE: "Precorics Member",

    // JOIN-MESSAGE
    FACTION_JOIN_MSG_ID: "899713824570081340",

    // CHANNEL ID'S
    BOT_COMMANDS_CHANNEL_ID: "894953858843959408",
    JOIN_FACTION_CHANNEL_ID: "887991030383771698",
    FACTION_1_VOTING_CHANNEL_ID: "887991226647863387",

    FACTION_1_JOIN_REACTION: "<:operativeunited_logo:899706423792005140>",

    GUILD_ID: "884463636012212254",
    TOKEN: "OTA0NjkxNDg4ODU0ODAyNDUz.YX_Nmw.pfTK5V_Pzz1HV5Jk4g2x_lAT0ME",

    VOTE_COMMAND_OBJ: {
        "command_name":"create-vote",
        "vote_id":420,
        "vote_faction":1,
        "vote_duration":1,
        "vote_title":"What shall we construct next?",
        "vote_thumbnail_url":null,
        "vote_choices":[
            {
                "id":1,
                "name":"Build a Space elevator",
                "time_limit":120,
                "resources":[

                ],
                "image_url":null
            },
            {
                "id":3,
                "name":"Develop blueprints for a planet harvester",
                "time_limit":120,
                "resources":[

                ],
                "image_url":null
            },
            {
                "id":2,
                "name":"Build a spaceship",
                "time_limit":120,
                "resources":[

                ],
                "image_url":null
            }
        ]
    }
};


module.exports = config
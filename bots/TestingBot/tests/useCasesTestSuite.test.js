const {Client, Intents } = require('discord.js');
const config = require("../config/config.js");

const client = new Client({
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
        Intents.FLAGS.GUILD_MEMBERS
    ],
    partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
});

jest.setTimeout(config.VOTE_COMMAND_OBJ.vote_duration * 60 * 1000 + 5000)


beforeAll(() => {
    // Remove all faction member roles and add Recruit role
    return client.login(config.TOKEN).then(t => {
        return client.guilds.fetch(config.GUILD_ID).then(guild => {
            return guild.roles.fetch().then(guildRoles => {
                let recruitRole = guildRoles.find(role => role.name === config.FACTION_RECRUIT_ROLE)
                let faction1Role = guildRoles.find(role => role.name === config.FACTION_1_ROLE)
                let faction2Role = guildRoles.find(role => role.name === config.FACTION_2_ROLE)
                let faction3Role = guildRoles.find(role => role.name === config.FACTION_3_ROLE)
                return guild.members.fetch(client.user.id).then(guildMember => {
                    return guildMember.roles.remove([faction1Role, faction2Role, faction3Role]).then(x => {
                        return guildMember.roles.add(recruitRole)
                    })
                })
            })

        })
    })
});

afterAll(() => {
    return client.destroy()
})


test('Tests the functionality for joining faction 1 (Operative United)', () => {
    return client.guilds.fetch(config.GUILD_ID).then(guild => {
        return guild.members.fetch(client.user.id).then(guildMember => {
            let recruitRole = guildMember.roles.cache.find(role => role.name === config.FACTION_RECRUIT_ROLE)
            expect(recruitRole).not.toBe(undefined)
            return guild.channels.fetch(config.JOIN_FACTION_CHANNEL_ID).then(channel => {
                return channel.messages.fetch(config.FACTION_JOIN_MSG_ID).then(joinMessage => {
                    return joinMessage.react(config.FACTION_1_JOIN_REACTION).then(r => {
                        return sleep(1000).then(() => {
                            return guild.members.fetch(client.user.id).then(guildMember => {
                                let correctRole = guildMember.roles.cache.find(role => role.name === config.FACTION_1_ROLE)
                                expect(correctRole).not.toBe(undefined)
                            })
                        })
                    })
                })
            })
        })
    })
});


test('Tests the functionality of voting for a mission (in Operative United company)', () => {
    return client.guilds.fetch(config.GUILD_ID).then(guild => {
        return guild.channels.fetch(config.BOT_COMMANDS_CHANNEL_ID).then(botCommandsChannel => {
            botCommandsChannel.send(JSON.stringify(config.VOTE_COMMAND_OBJ))
            return sleep(1000).then(() => {
                return guild.channels.fetch(config.FACTION_1_VOTING_CHANNEL_ID).then(factionVotingChannel => {
                    return factionVotingChannel.messages.fetch( {limit: 1}).then(messages1 => {
                        let voteMessage = messages1.first()
                        expect(voteMessage.embeds.length).toBe(4)
                        voteMessage.react("1️⃣")
                        return sleep(config.VOTE_COMMAND_OBJ.vote_duration * 60 * 1000 + 1000).then(() => {
                            factionVotingChannel.messages.fetch( {limit: 1} ).then(messages2 => {
                                let resultsMessage = messages2.first()
                                expect(resultsMessage.content.startsWith("**Final vote results**:\n")).toBe(true)
                            })
                        })
                    })
                })
            })
        })
    })
})

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
const dotenv = require('dotenv');
const axios = require("axios");
const config = require("../config/config");
const {MessageEmbed, MessageActionRow, MessageButton} = require("discord.js");
dotenv.config()


let activeMissionMsgId;
let activeMissionId;

const factionNumber = parseInt(process.env.factionNumber)


module.exports = {
    name: 'ready',
    once: true,
    execute(client) {
        if (process.env.LOG){
            console.log(`Ready! Logged in as ${client.user.tag}`);
            console.log(`Faction number: ${factionNumber}`)
        }

        if (!config.FACTION_INTRO_MESSAGE_IDS[factionNumber]) {
            createFactionIntroMessage(client)
        }

        setInterval(() => displayActiveMissionStats(client), config.ACTIVE_MISSION_UPDATE_INTERVAL * 1000)
        displayActiveMissionStats(client)
    },
};

function displayActiveMissionStats(client) {
    axios.get(config.ACTIVE_MISSIONS_ENDPOINT_URL).then(async response => {
        let activeMissionsData = response.data
        let activeMissionData
        for (const missionId in activeMissionsData) {
            let mission = activeMissionsData[missionId]
            if (mission.faction_id === factionNumber) {
                activeMissionData = mission
                break
            }
        }
        if (!activeMissionData) return

        if (!activeMissionData.name) activeMissionsData.name = "."
        if (!activeMissionData.description) activeMissionData.description = "."

        let guild = await client.guilds.fetch(config.GUILD_ID)
        let voteChannel = await guild.channels.fetch(config.FACTION_VOTE_CHANNEL_IDS[factionNumber])
        if (!activeMissionMsgId || activeMissionId !== activeMissionData.id) {
            // send new message
            let placeholderEmbed = new MessageEmbed().setDescription(".")
            let message = await voteChannel.send({embeds: [placeholderEmbed]})
            activeMissionMsgId = message.id
            activeMissionId = activeMissionData.id
        }

        let activeMissionMsg = await voteChannel.messages.fetch(activeMissionMsgId)
        let missionEmbeds = getActiveMissionEmbeds(activeMissionData)
        await activeMissionMsg.edit({embeds: missionEmbeds})

    }).catch(error => console.log(error))
}

function getActiveMissionEmbeds(missionData) {
    let mainEmbed = new MessageEmbed()
        .setTitle("Current active mission")
        .setImage(missionData.image)
        .addField(missionData.name, missionData.description)
        .addField("Time left: ", getTimeStr(missionData.time_left))

    let resourceEmbed = new MessageEmbed()
        .setTitle("Gathered resources")
    for (const resource of missionData.resources) {
        let collectedAmount = resource.collected ? resource.collected : 0
        let requiredAmount = resource.specified_amount
        let amountsString = collectedAmount.toString() + " / " + requiredAmount.toString()

        let resourceNameText = config.RESOURCE_EMOJIS[resource.name] + " " + resource.name
        resourceEmbed.addField(resourceNameText, amountsString)
    }

    return [mainEmbed, resourceEmbed]
}

function getTimeStr(minutes) {
    if (minutes <= 0) return "Time's up!"
    let days = Math.floor(minutes / 60 / 24)
    let hours = Math.floor(minutes / 60)
    let remainingMins = minutes % 60
    return days + " d " + hours + " h " + remainingMins + " m"
}

function createFactionIntroMessage(client) {
    client.guilds.fetch(config.GUILD_ID).then(guild => {
        guild.channels.fetch(config.FACTION_INTRO_CHANNEL_IDS[factionNumber]).then(introChannel => {
            let row = new MessageActionRow()
                .addComponents(
                    new MessageButton()
                        .setCustomId(config.LEAVE_FACTION_BUTTON_CUSTOM_IDS[factionNumber])
                        .setLabel("Leave faction")
                        .setStyle("DANGER")
                )
            introChannel.send( {content: config.FACTION_INTRO_TEXTS[factionNumber], components: [row]})
        })
    })
}
const config = require("../config/config.js");

const factionNumber = parseInt(process.env.factionNumber)


module.exports = {
    name: 'interactionCreate',
    execute(interaction) {
        if (interaction.isButton()) {
            console.log("Button pressed")
            if (interaction.customId === config.LEAVE_FACTION_BUTTON_CUSTOM_IDS[factionNumber]) {
                removeUserFromFaction(interaction)
                interaction.deferUpdate()
            }
        }
    }
}

function removeUserFromFaction(interaction) {
    console.log(`Removing user ${interaction.user.username} from faction nr ${factionNumber}`)
    let factionRole = interaction.guild.roles.cache.find(r => r.name === config.FACTION_ROLES[factionNumber])
    let recruitRole = interaction.guild.roles.cache.find(r => r.name === config.RECRUIT_ROLE)
    interaction.member.roles.remove(factionRole)
    interaction.member.roles.add(recruitRole)
}
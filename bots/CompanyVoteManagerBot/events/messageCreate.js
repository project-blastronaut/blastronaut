const config = require("../config/config.js");
const dotenv = require('dotenv');
const axios = require('axios')
const {createHash} = require('crypto');
const {MessageEmbed} = require('discord.js');
dotenv.config();

const factionNumber = parseInt(process.env.factionNumber)
const secret = process.env.secret

const voteReactions = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟"]
const factionColours = {"F1": "#ffb300", "F2": "#0073de", "F3": "#43b800"}

let voteInfo = []

module.exports = {
    name: 'messageCreate',
    execute(message) {
        if (message.channelId === config.BOT_COMMANDS_CHANNEL_ID) {
            handleServerCommand(message)
        }

    }
}

function handleServerCommand(message) {
    let strippedMessage = message.content.replace("```json", "").replace("```", "")
    let commandObj
    try {
        commandObj = JSON.parse(strippedMessage)
    } catch (SyntaxError) {
        console.log("Invalid command: " + message.content)
        return
    }

    if (commandObj.command_name === "create-vote" && commandObj.vote_faction === factionNumber) {
        createVote(message, commandObj)
    } else if (commandObj.command_name === "resource_sent" && commandObj.faction_id === factionNumber) {
        displaySentResource(message, commandObj)
    }
}


function createVote(message, commandObj) {
    let voteId = commandObj.vote_id
    let voteDuration = commandObj.vote_duration
    let voteTitle = commandObj.vote_title
    let voteThumbnailURL = commandObj.vote_thumbnail_url
    let voteChoices = commandObj.vote_choices
    let voteFaction = factionNumber

    let placeholder = new MessageEmbed().setTitle("**Vote starting soon**")
    message.guild.channels.fetch(config.FACTION_VOTE_CHANNEL_IDS[factionNumber]).then(channel => {
        channel.send({embeds: [placeholder]}).then(voteMessage => {
            let validChoices = []
            let choiceEmbeds = []
            for (let i = 0; i < voteChoices.length; i++) {
                let choice = voteChoices[i]
                let id = choice.id
                let choiceName = choice.name
                let choiceImageURL = choice.image_url
                let emoji = voteReactions[i]
                let choiceTimeLimit = choice.time_limit
                let choiceResources = choice.resources
                validChoices[emoji] = [id, choiceName]
                voteMessage.react(emoji)

                choiceEmbeds.push(getVoteChoiceEmbed(emoji, choiceName, choiceImageURL, choiceTimeLimit, choiceResources))
            }

            console.log(validChoices)

            let voteData = {
                "vote-msg-id": voteMessage.id,
                "vote-id": voteId,
                "valid-choices": validChoices,
                "remaining-duration": voteDuration,
                "faction": voteFaction,
                "vote-title": voteTitle,
                "thumbnail-link": voteThumbnailURL,
                "vote-interval-id": null,
                "choice-embeds": choiceEmbeds
            }

            let voteMainEmbed = getVoteMainEmbed(voteTitle, voteThumbnailURL, voteFaction, voteDuration)
            let embeds = [voteMainEmbed].concat(choiceEmbeds)

            voteMessage.edit({embeds: embeds})
            voteData["vote-interval-id"] = setInterval(() => voteCountdown(voteMessage, voteData), 60000)

            voteInfo.push(voteData)
        })
    })
}

function voteCountdown(voteMessage, voteData) {
    voteData["remaining-duration"] -= 1
    let voteMainEmbed = getVoteMainEmbed(voteData["vote-title"], voteData["thumbnail-link"], voteData["faction"], voteData["remaining-duration"])
    let embeds = [voteMainEmbed].concat(voteData["choice-embeds"])
    voteMessage.edit({embeds: embeds})
    if (voteData["remaining-duration"] <= 0) {

        let voteDataReactions = voteData["valid-choices"]

        let replyText = "**Final vote results**:\n"
        let winner;
        let winnerId;
        let winnerVotes = -1;

        voteMessage.reactions.cache.forEach(reaction => {
            let reactionInfo = voteDataReactions[reaction._emoji.name]
            let reactionCount = reaction.count - 1
            let choiceName = reactionInfo[1]
            replyText += choiceName + ": " + reactionCount + " votes!\n"
            if (reactionCount > winnerVotes) {
                winner = choiceName
                winnerId = reactionInfo[0]
                winnerVotes = reactionCount
            }
        })

        replyText += "\nThe winner is " + winner
        voteMessage.reply(replyText)
        voteInfo = voteInfo.filter(voteData => voteData["vote-msg-id"] !== voteMessage.id) // remove ended vote

        let data = {
            vote_id: voteData["vote-id"],
            winner_id: winnerId,
            secret: secret
        }
        const hash = createHash('sha256').update(JSON.stringify(data)).digest('hex');
        let requestBody = {
            vote_id: voteData["vote-id"],
            winner_id: winnerId,
            hash: hash,
        };


        axios.post(config.VOTE_RESULTS_ENDPOINT_URL, requestBody)
            .then(response => console.log(response))
            .catch(error => console.log(error))

        clearInterval(voteData["vote-interval-id"]);

    }

}

function getVoteMainEmbed(title, thumbnailLink, voteFaction, timeLeft) {
    return new MessageEmbed()
        .setTitle(title)
        .setDescription("Open vote for all company members")
        .setThumbnail(thumbnailLink)
        .addField("Time left: ", getTimeStr(timeLeft), true)
        .setColor(factionColours[voteFaction])
}

function getVoteChoiceEmbed(choiceEmoji, choiceName, choiceImage, choiceTimeLimit, choiceResources) {
    let choiceEmbed = new MessageEmbed()
        .setTitle(choiceEmoji + "   " + choiceName)
        .setDescription("Time limit: " + getTimeStr(choiceTimeLimit))
        .setImage(choiceImage)
    for (let resourceName in choiceResources) {
        let resourceNameText = config.RESOURCE_EMOJIS[resourceName] + " " + resourceName
        let resourceAmount = choiceResources[resourceName].toString()
        choiceEmbed.addField(resourceNameText, resourceAmount)
    }
    return choiceEmbed
}

function getTimeStr(minutes) {
    if (minutes <= 0) return "Time's up!"
    let days = Math.floor(minutes / 60 / 24)
    let hours = Math.floor(minutes / 60)
    let remainingMins = minutes % 60
    return days + " d " + hours + " h " + remainingMins + " m"
}


function displaySentResource(message, commandObj) {
    let messageText

    let resourceText = commandObj.resource_name
    if (config.RESOURCE_EMOJIS[commandObj.resource_name]) resourceText += " " + config.RESOURCE_EMOJIS[commandObj.resource_name]

    let playerId = commandObj.player_id
    if (playerId !== "unknown") {
        messageText = `<@${playerId}> has gathered **${commandObj.quantity} ${resourceText}** for our current mission **${commandObj.mission_name}** and received ${commandObj.credits} credits!`
    } else {
        if (commandObj.credits < config.MIN_CREDITS_FOR_NOTIFICATION) return
        messageText = `An unknown individual has gathered **${commandObj.quantity} ${resourceText}** for our current mission **${commandObj.mission_name}**!`
    }

    message.guild.channels.fetch(config.FACTION_CHAT_CHANNEL_IDS[factionNumber]).then(chatChannel => {
        chatChannel.send(messageText)
    })
}
const config = {

    GUILD_ID: "884463636012212254",

    FACTION_ROLES: {
      1: "Operative United Member",
      2: "Vector Accelerate Member",
      3: "Precorics Member"
    },
    RECRUIT_ROLE: "Recruit",

    FACTION_VOTE_CHANNEL_IDS: {
        1: "917155368407277588",
        2: "917155436275327016",
        3: "917156155267096636"
    },

    FACTION_CHAT_CHANNEL_IDS: {
        1: "917155422182461440",
        2: "917155464825962546",
        3: "917156631714889798"
    },

    FACTION_INTRO_CHANNEL_IDS: {
        1: "913853333029130310",
        2: "913856248431149156",
        3: "913856309827358821"
    },

    FACTION_INTRO_TEXTS: {
        1: "Welcome to **Operative United**!\nWe are a company formed from honest and hard-working common people. Everyone is equal in Operative United and we always help our brothers and sisters in need!",
        2: "Welcome to **Vector Accelerate**!\nVector Accelerate is without doubt the most scientifically and technologically advanced company out there. Every day we strive to create new inventions to make humanity's life better, with our great founder Jonas Hermann Grant at the forefront!",
        3: "Welcome to **Precorics**!\nIt can be said with absolute certainty that Precorics does not have any human weaknesses. That's because Precorics is governed by the most intelligent AI in the universe, the Artificial Neuromantic HyperBrain 2.0!",
    },

    FACTION_INTRO_MESSAGE_IDS: {
        1: "915227675184988201",
        2: "915227568720977971",
        3: "915227441780363265"
    },

    LEAVE_FACTION_BUTTON_CUSTOM_IDS: {
        1: "leave-faction-1",
        2: "leave-faction-2",
        3: "leave-faction-3"
    },

    BOT_COMMANDS_CHANNEL_ID: "894953858843959408",

    VOTE_RESULTS_ENDPOINT_URL: "https://blastronaut.optimatica.eu/api/discordBot/winningMission",
    ACTIVE_MISSIONS_ENDPOINT_URL: "https://blastronaut.optimatica.eu/api/activemissions",

    ACTIVE_MISSION_UPDATE_INTERVAL: 30, // seconds

    MIN_CREDITS_FOR_NOTIFICATION: 100,

    RESOURCE_EMOJIS: {
        "Aberrolite": "<:Aberollite:913787602753163336>",
        "Baddinum": "<:Baddinum:913787666972155955>",
        "Cristal": " <:Cristal:913787667223810098>",
        "Dolicite": "<:Dolicitite:913787667190255627>",
        "Ferortsite": "<:Ferortsite:913787667261583450>",
        "Gmer": "<:Gmer:913787667228012635>",
        "Hellote": "<:Hellote:913787667295109120>",
        "Iolie": "<:Iolie:913787731480547378>",
        "Leaum": "<:Leaum:913787731476365362>",
        "Mushroom": "<:Mushroom:913787731455377468>",
        "Niero": "<:Niero:913787731572846632>",
        "Norgar": "<:Norgar:913787731216306219>",
        "Rambiz": "<:Rambiz:913787731174359111>",
        "Rhodmannite": "<:Rhodmannite:913787731472154624>",
        "Speorl": "<:Speorl:913787731556052992>",
        "Weed": "<:Weed:913787731476348929>"
    },
};

module.exports = config
const config = {

    BOT_COMMANDS_CHANNEL_ID: "894953858843959408",

    FACTION_SOCIAL_MISSIONS_CHANNEL_IDS: {
        1: "917155399168323614",
        2: "917155449319596082",
        3: "917155827436093440",
        4: "917155526205378601"
    },

    CONVERSATION_INTERVAL_TIME: 666, // minutes

    FACTION_SOCIAL_MISSION_INITIATION_TEXTS: {
        1: [
            "Good day, fellow workers! To keep up the motivation of you all, we've decided to start a little challenge.",
            "How's it going, my good people? Me and some of our fellow brothers and sisters thought it'd be fun to have a little friendly competition.",
            "Good day to you all, comrades! I'd hate to interrupt your honest work, but I thought I'd let you all know we've started a small competition."
        ],
        2: [
            "Greetings, citizens! Our great leader Jonas Hermann Grant has graced us with a social challenge.",
            "Attention, employees! We are hereby presenting you a new social challenge, as follows:",
            "Glory to Our Founder Grant! To hold the company's honour high, I am announcing a new social challenge."
        ],
        3: [
            "Greetings, dear humans! Statistics show that a little social competition helps to keep employees productive, so here is a small challenge.",
            "Hello, employees! Management has decided that it would be nice to start a new and exciting social competition.",
            "Hey, people! I'm happy to announce to you that we will be starting a new social challenge!"
        ],
        4: [
            "Greetings to everyone! The United Commission of Interplanetary Associations is hereby announcing that a social challenge will be held among Operative United, Vector Accelerate and Precorics.",
            "Attention, citizens of the galaxy! I am pleased to announce to you all that a new social competition is now being started.",
            "Good day to everyone! The Intergalactic Workers' Association has asked me to find out which company's employees are the most innovative and creative. In order to do that, I am asking every company member to take part in this competition.",
        ],
    },

    FACTION_SOCIAL_MISSION_NO_SUBMISSIONS_TEXTS: {
        1: "Looks like no-one has has made any submissions... I'm honestly disappointed in you all, comrades. This is a sad day in the history of our company.",
        2: "It seems that no-one has has made any submissions... This is an unacceptable display of lack of morale in our company. I hope to never again see it repeated!",
        3: "Strange, it seems that no-one has mady any submissions. My programming has not prepared me for this circumstance...",
        4: "Looks like not a single submission has been made... I would never have believed that my eyes would see such a display of inactivity from Operative United, Vector Accelerate and Precorics."
    },

    SOCIAL_MISSION_POINTS_MULTIPLIER: 10, // likes * MULTIPLIER = points

    CREDO_ENDPOINT_URL: "https://blastronaut.optimatica.eu/api/discordBot/credo",
};

module.exports = config
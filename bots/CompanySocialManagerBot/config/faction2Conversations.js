const faction2Conversations = [
    {
        participants: {
            0: "https://discord.com/api/webhooks/917349480234364968/2OANr1GXt7UkNQwswRUKNLAH6NYgCU86b-MKM9hZIPcL7wh6Pbmkip_EzE28Dq51N94q",
            1: "https://discord.com/api/webhooks/917349640020557904/2cUIIbY0gmPxaKSljQhq_sosncHtR9YbtoaCItpitAese4BaCZBB3LPTF80gkypUGZIZ"
        },
        messages: [
            {
                sender: 0,
                text: "I'm going to have a cup of coffee, anyone want to join?",
                reply_time: 20
            },
            {
                sender: 1,
                text: "I would join if you don't mind! Coffee would be nice to wake me up.",
                reply_time: 7
            },
            {
                sender: 0,
                text: "That would be nice! Meet me at the buffet in 5 minutes?",
                reply_time: 10
            },
            {
                sender: 1,
                text_variants: ["Sure", "Sounds great!", "I'll be there!"],
                reply_time: null
            }
        ]
    },
    {
        participants: {
            0: "https://discord.com/api/webhooks/917349480234364968/2OANr1GXt7UkNQwswRUKNLAH6NYgCU86b-MKM9hZIPcL7wh6Pbmkip_EzE28Dq51N94q",
            1: "https://discord.com/api/webhooks/917349640020557904/2cUIIbY0gmPxaKSljQhq_sosncHtR9YbtoaCItpitAese4BaCZBB3LPTF80gkypUGZIZ"
        },
        messages: [
            {
                sender: 0,
                text: "Has anyone heard anything about what those AI slaves in Precorics are constructing?",
                reply_time: 10
            },
            {
                sender: 1,
                text: "I heard they are trying to build some kind of a space elevator",
                reply_time: 8
            },
            {
                sender: 0,
                text: "Sounds like a stupid idea",
                reply_time: 7
            },
            {
                sender: 1,
                text: "Yeah, that's what i thought as well",
                reply_time: 9
            },
            {
                sender: 0,
                text: "Good thing we don't waste resources on such nonsense",
                reply_time: null
            }
        ]
    }
]

module.exports = faction2Conversations
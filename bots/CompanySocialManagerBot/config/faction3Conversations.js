const faction3Conversations = [
    {
        participants: {
            0: "https://discord.com/api/webhooks/917349780731084841/USqUSXNLOk5-mWx8UNwdVs9RSxgEFrsjV-dR7P8-e9_yK44JrWqLhgo8pyObhaKXANvb",
            1: "https://discord.com/api/webhooks/917349884787556402/UhVHCaeJYprLH8DvwCjmgCSvaUUCYhL_OEoSkkPz86DnQVV-E6E_iwjNpG0GXAE7SIEY"
        },
        messages: [
            {
                sender: 0,
                text: "I'm going to have a cup of coffee, anyone want to join?",
                reply_time: 20
            },
            {
                sender: 1,
                text: "I would join if you don't mind! Coffee would be nice to wake me up.",
                reply_time: 7
            },
            {
                sender: 0,
                text: "That would be nice! Meet me at the buffet in 5 minutes?",
                reply_time: 10
            },
            {
                sender: 1,
                text: "Sure!",
                reply_time: null
            }
        ]
    },
    {
        participants: {
            0: "https://discord.com/api/webhooks/917349780731084841/USqUSXNLOk5-mWx8UNwdVs9RSxgEFrsjV-dR7P8-e9_yK44JrWqLhgo8pyObhaKXANvb",
            1: "https://discord.com/api/webhooks/917349884787556402/UhVHCaeJYprLH8DvwCjmgCSvaUUCYhL_OEoSkkPz86DnQVV-E6E_iwjNpG0GXAE7SIEY"
        },
        messages: [
            {
                sender: 0,
                text: "Hey everyone! You won't believe what I found yesterday when I was mining in the new excavation area!",
                reply_time: 12
            },
            {
                sender: 1,
                text: "Well what did you find then this time?",
                reply_time: 5
            },
            {
                sender: 0,
                text: "Well it looked like some kind of a metal, but very shiny and almost glowing with light!",
                reply_time: 10
            },
            {
                sender: 1,
                text: "Huh. Sounds interesting, come here and show it to me.",
                reply_time: 9
            },
            {
                sender: 0,
                text: "Well... I couldn't pick it up because my fuel was getting real low.",
                reply_time: 13
            },
            {
                sender: 1,
                text: "Why are you even wasting our time then with this nonsense if you don't even have it?",
                reply_time: 7
            },
            {
                sender: 0,
                text: "Well I just thought you would be interested",
                reply_time: 5
            },
            {
                sender: 1,
                text: "Well i'm not anymore",
                reply_time: null
            }
        ]
    }
]

module.exports = faction3Conversations
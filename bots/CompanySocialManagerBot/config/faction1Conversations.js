const faction1Conversations = [
    {
        participants: {
            0: "https://discord.com/api/webhooks/917348937898274847/PYOtmYDtkgfra8NS9u-Vm7KR3cwj2Qe6AD0Cb16q0uJ2ALJ24cPmSh-UlV3JEeo2J3pG",
            1: "https://discord.com/api/webhooks/917349348067667979/oUydf0myPlo1eRyH6cQorLzRKxO1YP9DVMSFSmZFztq2BAgvj1vyyBN6AgDwJVsenk6r"
        },
        messages: [
            {
                sender: 0,
                text: "I'm going to have a cup of coffee, anyone want to join?",
                reply_time: 20
            },
            {
                sender: 1,
                text: "I would join if you don't mind! Coffee would be nice to wake me up.",
                reply_time: 7
            },
            {
                sender: 0,
                text: "That would be nice! Meet me at the buffet in 5 minutes?",
                reply_time: 10
            },
            {
                sender: 1,
                text_variants: ["Sure", "Sounds great!", "I'll be there!"],
                reply_time: null
            }
        ]
    },
    {
        participants: {
            0: "https://discord.com/api/webhooks/917348937898274847/PYOtmYDtkgfra8NS9u-Vm7KR3cwj2Qe6AD0Cb16q0uJ2ALJ24cPmSh-UlV3JEeo2J3pG",
            1: "https://discord.com/api/webhooks/917349348067667979/oUydf0myPlo1eRyH6cQorLzRKxO1YP9DVMSFSmZFztq2BAgvj1vyyBN6AgDwJVsenk6r"
        },
        messages: [
            {
                sender: 0,
                text: "Hey Joe, what do you think about the new project that won the voting?",
                reply_time: 24
            },
            {
                sender: 1,
                text: "I don't know, I would have liked the Space Station one better.",
                reply_time: 8
            },
            {
                sender: 0,
                text: "Why is that? I think this one is pretty cool!",
                reply_time: 9
            },
            {
                sender: 1,
                text: "Well it seems to me that a new space station would benefit us more.",
                reply_time: 7
            },
            {
                sender: 0,
                text: "I guess you have a point",
                reply_time: null
            }
        ]
    }
]

module.exports = faction1Conversations
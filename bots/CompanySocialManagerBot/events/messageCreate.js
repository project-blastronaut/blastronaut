const config = require("../config/config.js");
const dotenv = require('dotenv');
const axios = require('axios')
const { createHash } = require('crypto');
const { MessageEmbed } = require('discord.js');
dotenv.config();

const factionNumber = parseInt(process.env.factionNumber)
const secret = process.env.secret

let activeSocialChallenges = {}


module.exports = {
    name: 'messageCreate',
    execute(message) {
        if (message.channelId === config.BOT_COMMANDS_CHANNEL_ID) {
            handleServerCommand(message)
        } else if (message.channelId in activeSocialChallenges) {
            handleSocialChallengeSubmission(message)
        }

    }
}

function handleServerCommand(message) {
    let strippedMessage = message.content.replace("```json", "").replace("```", "")
    let commandObj
    try {
        commandObj = JSON.parse(strippedMessage)
    } catch (SyntaxError) {
        console.log("Invalid command: " + message.content)
        return
    }

    console.log(commandObj)

    if (commandObj.command_name === "create-social-mission" && commandObj.mission_faction === factionNumber) {
        createSocialMission(message, commandObj)
    }

}

function randomChoice(list) {
    let index = Math.floor(Math.random() * list.length)
    return list[index]
}

function createSocialMission(message, commandObj) {
    let missionId = commandObj.mission_id
    let missionDuration = commandObj.mission_duration
    let missionText = commandObj.mission_text
    let missionThumbnailURL = commandObj.mission_thumbnail_url

    let messageText = randomChoice(config.FACTION_SOCIAL_MISSION_INITIATION_TEXTS[factionNumber])

    let missionTitle = "Social Challenge " + missionId
    let missionEmbed = getSocialMissionEmbed(missionTitle, missionText, missionDuration, missionThumbnailURL)
    message.guild.channels.fetch(config.FACTION_SOCIAL_MISSIONS_CHANNEL_IDS[factionNumber]).then(channel => {
        channel.send({content: messageText, embeds: [missionEmbed]}).then(missionMessage => {
            channel.threads.create({name: missionTitle, reason: "For challenge submissions", autoArchiveDuration: 1440}).then(threadChannel => {
                threadChannel.send("Post your challenge submissions here! For them to be counted, start your submission message with #{this thread}.")
                let missionData = {
                    "mission-id": missionId,
                    "mission-msg-id": missionMessage.id,
                    "mission-thread-id": threadChannel.id,
                    "title": missionTitle,
                    "text": missionText,
                    "thumbnail-url": missionThumbnailURL,
                    "remaining-duration": missionDuration,
                    "submissions": {
                        // userId: messageId
                    },
                    "mission-interval-id": null
                }

                missionData["mission-interval-id"] = setInterval(() => socialMissionCountdown(missionMessage, missionData), 60000)
                activeSocialChallenges[threadChannel.id] = missionData
            })
        })
    })
}

function socialMissionCountdown(missionMessage, missionData) {
    missionData["remaining-duration"] -= 1

    let embed = getSocialMissionEmbed(missionData["title"], missionData["text"], missionData["remaining-duration"], missionData["thumbnail-url"])
    missionMessage.edit({embeds: [embed]})

    if (missionData["remaining-duration"] <= 0) {
        missionMessage.guild.channels.fetch(missionData["mission-thread-id"]).then(async threadChannel => {
            let submissions = missionData["submissions"]
            let submissionLikes = []
            for (const userId in submissions) {
                let msgId = submissions[userId]
                let msg = await threadChannel.messages.fetch(msgId)
                let thumbsUpReaction = msg.reactions.cache.find(reaction => reaction.emoji.name === "👍")
                if (thumbsUpReaction === undefined) {
                    console.log("The message does not seem to have a 'like' reaction attached...")
                    submissionLikes.push([userId, 0])
                } else {
                    submissionLikes.push([userId, thumbsUpReaction.count])
                }
            }

            let top5Submissions = submissionLikes.sort((a, b) => b[1] - a[1]).slice(0, 5)

            let missionResultText = "Time's up for " + missionData["title"] + "! The top submissions are as follows:\n"
            let i = 1
            top5Submissions.forEach(submission => {
                let userId = submission[0]
                let likeCount = submission[1]
                let userPing = "<@" + userId + ">"
                missionResultText += i + ". " + userPing + ": " + likeCount + " likes\n"
                i++

                let points = likeCount * config.SOCIAL_MISSION_POINTS_MULTIPLIER

                // send request to server to give points
                let data = {
                    player_id: userId,
                    amount: points,
                    secret: secret
                }
                const hash = createHash('sha256').update(JSON.stringify(data)).digest('hex');
                let requestBody = {
                    player_id: userId,
                    amount: points,
                    hash: hash,
                };

                axios.post(config.CREDO_ENDPOINT_URL, requestBody)
                    .then(response => console.log(response))
                    .catch(error => console.log(error))
            })

            if (submissionLikes.length > 0) {
                let winnerId = top5Submissions[0][0]
                let winnerPing = "<@" + winnerId + ">"
                missionResultText += "\nCongratulations, " + winnerPing + ", you are this Social challenge's winner!"
                missionMessage.reply(missionResultText)
            } else {
                missionMessage.reply("Time's up for " + missionData["title"] + "!\n" + config.FACTION_SOCIAL_MISSION_NO_SUBMISSIONS_TEXTS[factionNumber])
            }


            delete activeSocialChallenges[missionData["mission-thread-id"]] // remove ended mission
            clearInterval(missionData["mission-interval-id"])
        })

    }
}

function handleSocialChallengeSubmission(message) {
    let validTag = "<#" + message.channelId + ">"
    if (message.content.startsWith(validTag)) {
        // is submission
        let socialChallengeData = activeSocialChallenges[message.channelId]
        let submissions = socialChallengeData["submissions"]
        let authorId = message.author.id
        if (authorId in submissions) {
            // user has already submitted
            let authorPing = "<@" + authorId + ">"
            message.reply(authorPing + " you have already made a submission for this challenge! This one will now officially overwrite your previous submission.")
        }
        submissions[authorId] = message.id
        message.react("👍")
    }

}

function getSocialMissionEmbed(title, text, timeLeft, thumbnailURL) {
    let embed = new MessageEmbed()
        .setTitle(title)
        .setDescription(text)
        .addField("Time left: ", getTimeStr(timeLeft))
    if (thumbnailURL) embed.setThumbnail(thumbnailURL)
    return embed
}

function getTimeStr(minutes) {
    if (minutes <= 0) return "Time's up!"
    let days = Math.floor(minutes / 60 / 24)
    let hours = Math.floor(minutes / 60)
    let remainingMins = minutes % 60
    return days + " d " + hours + " h " + remainingMins + " m"
}
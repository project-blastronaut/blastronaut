const dotenv = require('dotenv');
const {WebhookClient} = require("discord.js");
const config = require("../config/config");
const faction1Conversations = require("../config/faction1Conversations");
const faction2Conversations = require("../config/faction2Conversations");
const faction3Conversations = require("../config/faction3Conversations");
dotenv.config()

let factionNumber = parseInt(process.env.factionNumber)

let conversationPool
switch (factionNumber) {
    case 1:
        conversationPool = faction1Conversations
        break
    case 2:
        conversationPool = faction2Conversations
        break
    case 3:
        conversationPool = faction3Conversations
        break
    case 4:
        conversationPool = null
        break
    default:
        throw Error("Invalid faction number in .env!")
}


module.exports = {
    name: 'ready',
    once: true,
    execute(client) {
        if (process.env.LOG){
            console.log(`Ready! Logged in as ${client.user.tag}`);
            console.log(`Faction number: ${process.env.factionNumber}`)
        }

        if (factionNumber === 4) return
        console.log("Setting conversation starting interval.")
        setInterval(() => startConversation(), config.CONVERSATION_INTERVAL_TIME * 60000)
    },
}

function startConversation() {
    console.log("Starting conversation.")
    let conversation = randomChoice(conversationPool)
    let webhooks = {}
    console.log(conversation.participants)
    for (const participant in conversation.participants) {
        webhooks[participant] = new WebhookClient({url: conversation.participants[participant]})
    }

    continueConversation(conversation.messages, webhooks, 0)
}

function continueConversation(messages, webhooks, i) {
    let message = messages[i]
    let webhook = webhooks[message.sender]
    if (message.text) webhook.send({content: message.text})
    else {
        let text = randomChoice(message.text_variants)
        webhook.send({content: text})
    }
    if (message.reply_time) {
        setTimeout(() => continueConversation(messages, webhooks, i+1), message.reply_time * 1000)
    }
}

function randomChoice(list) {
    let index = Math.floor(Math.random() * list.length)
    return list[index]
}
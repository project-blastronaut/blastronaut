const {SlashCommandBuilder} = require('@discordjs/builders');
const axios = require("axios");
const config = require("../config/config.js");

module.exports = {
    data: new SlashCommandBuilder()
        .setName('score')
        .setDescription("Gives user info about their score"),
    execute(interaction) {
        let guildMember = interaction.member
        let endpointURL = config.GET_PLAYER_CREDO_ENDPOINT + guildMember.id
        axios.get(endpointURL)
            .then(response => {
                let responseData = response.data
                if (responseData) {
                    interaction.reply(`Your score: ${responseData.credo} points`)
                } else {
                    interaction.reply("Your information is not present in the system.")
                }
            })
            .catch(error => {
                console.log("Error! Status code: " + error.response.status)
                console.log(error.response.data)
                interaction.reply("There was an error with the request, Blastronaut server might be temporarily down.")
            })
    }
}

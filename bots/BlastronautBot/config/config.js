const config = {

    GUILD_ID: "884463636012212254",

    // ROLES
    GAME_MASTER_ROLE: "Game Master",
    RECRUIT_ROLE: "Recruit",
    FACTION_1_ROLE: "Operative United Member",
    FACTION_2_ROLE: "Vector Accelerate Member",
    FACTION_3_ROLE: "Precorics Member",

    // JOIN-MESSAGE
    FACTION_JOIN_MSG_ID: "915967153130340432",

    // MISC CHANNEL ID'S
    BOT_COMMANDS_CHANNEL_ID: "894953858843959408",
    JOIN_FACTION_CHANNEL_ID: "887991030383771698",

    // REACTIONS
    FACTION_1_JOIN_REACTION: "<:operativeunited_logo:899706423792005140>",
    FACTION_2_JOIN_REACTION: "<:vectoraccelerate_logo:899706342422491147>",
    FACTION_3_JOIN_REACTION: "<:precorics_logo:899706167608102944>",

    UPDATE_PLAYER_ENDPOINT: "https://blastronaut.optimatica.eu/api/discordBot/updatePlayer",
    GET_PLAYER_CREDO_ENDPOINT: "https://blastronaut.optimatica.eu/api/discordBot/credo/"
};

module.exports = config
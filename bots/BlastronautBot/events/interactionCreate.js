const {Collection} = require('discord.js');
const fs = require('fs');

const commands = new Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require(`../commands/${file}`);
    commands.set(command.data.name, command);
}


module.exports = {
    name: 'interactionCreate',
    execute(interaction) {
        if (interaction.isButton()) {
            console.log("Button pressed")
        } else if (interaction.isCommand()) {
            const command = commands.get(interaction.commandName)
            if (command) {
                command.execute(interaction)
            }
        }
    }
}
const config = require("../config/config.js");
const dotenv = require('dotenv');
const {MessageEmbed} = require("discord.js");
dotenv.config()

module.exports = {
    name: 'ready',
    once: true,
    execute(client) {
        if (process.env.LOG) {
            console.log(`Ready! Logged in as ${client.user.tag}`);
        }

        if (!config.FACTION_JOIN_MSG_ID) {
            // create a new Faction Join Message
            client.guilds.fetch(config.GUILD_ID)
                .then(guild => {
                    guild.channels.fetch(config.JOIN_FACTION_CHANNEL_ID)
                        .then(channel => {
                            let embed = new MessageEmbed()
                                .setTitle("Join a faction")
                                .setDescription("Overview of the different factions: https://blastronaut.optimatica.eu/\n\n" +
                                    "To join one of the three factions, add a reaction to this message.\nEach faction has a corresponding reaction:")
                                .addField("Operative United", config.FACTION_1_JOIN_REACTION, true)
                                .addField("Vector Accelerate", config.FACTION_2_JOIN_REACTION, true)
                                .addField("Precorics", config.FACTION_3_JOIN_REACTION, true)
                            channel.send({embeds: [embed]})
                                .then(message => {
                                    message.react(config.FACTION_1_JOIN_REACTION)
                                    message.react(config.FACTION_2_JOIN_REACTION)
                                    message.react(config.FACTION_3_JOIN_REACTION)
                                    //config.FACTION_JOIN_MSG_ID = message.id
                                })
                        })
                })
        }
    },
};
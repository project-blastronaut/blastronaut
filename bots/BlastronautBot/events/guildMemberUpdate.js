const ServerCommunication = require("../common/ServerCommunication")

module.exports = {
    name: 'guildMemberUpdate',
    execute(oldMember, newMember) {
        ServerCommunication.updateGuildMemberInfo(newMember)
    }
}



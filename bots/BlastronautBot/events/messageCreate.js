const config = require("../config/config.js");
const dotenv = require('dotenv');
dotenv.config();


module.exports = {
    name: 'messageCreate',
    execute(message) {
        if (message.channelId === config.BOT_COMMANDS_CHANNEL_ID) {
            handleServerCommand(message)
        }

    }
}


function handleServerCommand(message) {
    let strippedMessage = message.content.replace("```json", "").replace("```", "")
    let commandObj
    try {
        commandObj = JSON.parse(strippedMessage)
    } catch (SyntaxError) {
        console.log("Invalid command: " + message.content)
        return
    }

    console.log(commandObj)

    let commandName = commandObj.command_name

}

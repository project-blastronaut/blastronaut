const ServerCommunication = require("../common/ServerCommunication")

module.exports = {
    name: 'userUpdate',
    execute(oldUser, newUser) {
        ServerCommunication.updateUserInfo(newUser)
    }
}
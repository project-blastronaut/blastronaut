const config = require("../config/config.js");
const dotenv = require('dotenv');
dotenv.config();

//When a person joins the guild (Discord server) he is assigned the recruit role, which allows him to see the introduction channel but not faction-specific channels.
module.exports = {
    name: 'guildMemberAdd',
    execute(member) {
        if (process.env.LOG) {
            console.log(`${member.user.username} joined the guild`);
        }
        let role = member.guild.roles.cache.find(r => r.name === config.RECRUIT_ROLE);
        member.roles.add(role)
    }
}
const config = require("../config/config.js");
const dotenv = require('dotenv');
dotenv.config();


module.exports = {
    name: 'messageReactionAdd',
    execute(reaction, user) {
        if (reaction.message.id === config.FACTION_JOIN_MSG_ID) {
            handleFactionJoinMsgReaction(reaction, user)
        }

    }
}


function handleFactionJoinMsgReaction(reaction, user) {
    let roleNameToAdd;
    let roleNamesToRemove;
    let emojiUnicode = "<:" + reaction.emoji.name + ":" + reaction.emoji.id + ">"
    switch (emojiUnicode) {
        case config.FACTION_1_JOIN_REACTION:
            console.log("Reaction for joining faction 1 was added to guild recruitment message");
            roleNameToAdd = config.FACTION_1_ROLE
            roleNamesToRemove = [config.RECRUIT_ROLE, config.FACTION_2_ROLE, config.FACTION_3_ROLE]
            break
        case config.FACTION_2_JOIN_REACTION:
            console.log("Reaction for joining faction 2 was added to guild recruitment message");
            roleNameToAdd = config.FACTION_2_ROLE
            roleNamesToRemove = [config.RECRUIT_ROLE, config.FACTION_1_ROLE, config.FACTION_3_ROLE]
            break
        case config.FACTION_3_JOIN_REACTION:
            console.log("Reaction for joining faction 3 was added to guild recruitment message");
            roleNameToAdd = config.FACTION_3_ROLE
            roleNamesToRemove = [config.RECRUIT_ROLE, config.FACTION_1_ROLE, config.FACTION_2_ROLE]
            break
        default:
            console.log("Illegal reaction was added to guild recruitment message");
            reaction.users.remove(user)
            return
    }
    let guild = reaction.message.guild
    let roleToAdd = guild.roles.cache.find(r => r.name === roleNameToAdd);
    let rolesToRemove = []
    for (const roleName of roleNamesToRemove) {
        let roleToRemove = guild.roles.cache.find(r => r.name === roleName);
        rolesToRemove.push(roleToRemove)
    }

    guild.members.fetch(user.id).then(async member => {
        await member.roles.remove(rolesToRemove);
        await member.roles.add(roleToAdd);
        await reaction.users.remove(user);
    })
}

const config = require("../config/config.js");
const {createHash} = require("crypto");
const axios = require("axios");

const secret = process.env.secret

module.exports = {
    updateGuildMemberInfo(guildMember) {
        let factionId = null
        if (guildMember.roles.cache.find(r => r.name === config.FACTION_1_ROLE)) factionId = 1
        else if (guildMember.roles.cache.find(r => r.name === config.FACTION_2_ROLE)) factionId = 2
        else if (guildMember.roles.cache.find(r => r.name === config.FACTION_3_ROLE)) factionId = 3

        let isGameMaster = guildMember.roles.cache.find(r => r.name === config.GAME_MASTER_ROLE) ? 1 : 0;

        let user = guildMember.user
        let avatarURL = user.displayAvatarURL({dynamic: true})

        let data = {
            discord_id: user.id,
            username: user.username,
            tag: user.tag,
            avatar_url: avatarURL,
            is_gm: isGameMaster,
            faction_id: factionId,
        }
        hashAndSend(data)
    },

    updateUserInfo(user) {
        let data = {
            discord_id: user.id,
            username: user.username,
            tag: user.tag,
            avatar_url: user.displayAvatarURL({dynamic: true}),
        }
        hashAndSend(data)
    }
}

function hashAndSend(data) {
    data.secret = secret
    let hash = createHash('sha256').update(JSON.stringify(data)).digest('hex');
    delete data.secret
    data.hash = hash

    console.log("Request data: ")
    console.log(data)
    axios.post(config.UPDATE_PLAYER_ENDPOINT, data)
        .then(response => {
            console.log("Response status code: " + response.status)
            console.log(response.data)
        })
        .catch(error => {
            console.log("Error! Status code: " + error.response.status)
            console.log(error.response.data)
        })
}

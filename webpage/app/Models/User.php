<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'faction_id',
        'discord_id',
        'image',
        "tag",
        "nickname",
    ];
    protected $attributes = [
        'image' => "https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png"
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function credoTransactions()
    {
        return $this->hasMany(CredoTransaction::class,'player_id');
    }

    public function providers()
    {
        return $this->hasMany(Provider::class,'user_id','id');
    }
}

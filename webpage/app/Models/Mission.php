<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Mission extends Model
{
    use HasFactory;
    protected $hidden = ['pivot'];
    public function requirements()
    {
        return $this->hasMany(MissionResourceRequirement::class)
            ->join('resources', 'resource_id', '=', 'resources.id');
    }

    public function requirement_points()
    {
        return $this->hasMany(MissionResourceRequirement::class)
            ->join('resources', 'resource_id', '=', 'resources.id');
    }

    public function points()
    {
        return $this->hasMany(MissionResourceRequirement::class)
            ->select('3 as points' );
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class, "mission_resource_requirement")
            ->withPivot('amount')
            ->select("resources.id", "resources.name", "resources.value", "resources.image");
    }

    
  

    protected $attributes = [
        'repeatable' => false,
    ];
    
    protected $fillable = [
        "resource_requireds_id",
        "name",
        "description",
        "image",
        "faction_id",
        "repeatable",
        "state",
        "time_limit",
        "starting_time"
    ];
};

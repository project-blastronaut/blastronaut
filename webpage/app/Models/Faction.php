<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Faction extends Model
{
    use HasFactory;
    protected $fillable = [
        "name",
        "description",
        "mission_time_minutes",
        "voting_time_minutes",
        "playerbase_size",
    ];

    public function missionCompletedTasks()
    {
        return $this->hasMany(Mission::class, 'faction_id')
            ->where("state","completed")
            ->with('requirements');
    }

    public function players()
    {
        return $this->hasMany(User::class, 'faction_id')
            ->select('id','name','image');

    }
  


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class CredoTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'quantity',
        'reason',
        'player_id',
        'faction_id',
    ];
}

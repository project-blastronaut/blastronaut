<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MissionResourceRequirement extends Model
{
    protected $table = 'mission_resource_requirement';
    use HasFactory;
    
    protected $fillable = [
        "mission_id",
        "amount",
        "specified_amount",
        "resource_id",
    ];

    public function resource()
    {
        return $this->belongsTo(Resource::class, 'resource_id');
    }

    public function mission() {
        return $this->belongsTo(Mission::class, 'mission_id');
    }
}

<?php

namespace App\Models;
use VotesMissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory;

    protected $fillable = [
        'state',
        "winning_mission_id",
        "title",
        "faction_id",
        "voting_time_minutes",
        "image",
    ];

    public function faction()
    {
        return $this->hasOne(Faction::class, "id", "faction_id");
    }

    public function missions()
    {
        return $this->belongsToMany(Mission::class, "vote_missions");
    }
}

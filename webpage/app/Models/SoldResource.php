<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class SoldResource extends Model
{
    use HasFactory;
    protected $fillable = [
        "resource_id",
        "quantity",
        'mission_id',
        'player_id',
    ];
}

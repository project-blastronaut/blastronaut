<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{   
    protected $hidden = ['pivot'];
    public function requirements()
    {
        return $this->hasOne(MissionResourceRequirement::class);
    }

    public function collected()
    {
        return $this->hasMany(SoldResource::class);
    }
    
    use HasFactory;
    protected $fillable = [
        "name",
        "image",
        "value",
    ];
}

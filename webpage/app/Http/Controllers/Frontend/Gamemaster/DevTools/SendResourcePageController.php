<?php 
namespace App\Http\Controllers\Frontend\Gamemaster\DevTools;
use App\Models\Mission;
use App\Models\Resource;
use App\Models\User;
use App\Models\SoldResource;
use App\Models\MissionResourceRequirement;
use App\Models\CredoTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Jobs\MissionLifecycle;
use Illuminate\Support\Facades\DB;


class SendResourcePageController extends Controller
{
    
    public function index()
    {   
        $missions = Mission::where("state","active")->get();
        $missions -> load("requirements");

        return Inertia::render('Gamemaster/DevTools/SendRes', [
            'missions' => $missions,
        ]);
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $request -> validate([
            'mission' =>"required",
            'player_id' =>"required|gt:0",
            "quantity" =>"required|gt:0",
            "resource_id" => "required"
        ]);

        DB::beginTransaction();

        $mission = Mission::find($request->mission);
        if (!$mission){
            DB::rollback();
            abort(400, "No such mission");
        }

        if ($mission->state!="active"){
            DB::rollback();
            abort(400, "Mission in not active");
        }
        
        $required =  MissionResourceRequirement::where('mission_id',$mission->id)->where('resource_id', $request->resource_id)->first()->specified_amount;
        $collected = SoldResource::where('mission_id',$mission->id)->where('resource_id', $request->resource_id)->sum('quantity');
     
        if ((int)$required < $collected + $request->quantity){
            DB::rollback();
            abort(400, "Can not send so many resources. Required: ".$required." Already collected: ". $collected." You tried to send ".$request->quantity);
        }
        if ((int)$required == $collected + $request->quantity){
            $mission->update([
                "state" => "completed",
            ]);
        }

        $player = User::where('discord_id', $request->player_id)->first();
        if (!$player) {
            $player = User::create([
                "faction_id" => $mission->faction_id,
                "discord_id" => $request->player_id,
            ]);
        }
        CredoTransaction::create([
            "faction_id" => $mission->faction_id,
            "player_id" => $player->id,
            "quantity" => $request->quantity,
            "reason" => "resource",
        ]);

        $sr = SoldResource::create([
            "mission_id" => $request->mission,
            'player_id' =>$player->id,
            "quantity" =>$request->quantity,
            "resource_id" => $request->resource_id,
        ]);
        
        DB::commit();
        return back();
    }  
} 
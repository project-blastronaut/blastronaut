<?php 
namespace App\Http\Controllers\Frontend\Gamemaster\DevTools;

use App\Http\Controllers\Controller;
use App\Models\MissionLifecyclePars;
use App\Models\Faction; 
use App\Models\Resource; 
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use App\Models\Mission;


class ActiveMissionsPageController extends Controller
{
    
    public function index()
    {   
        $missions = Mission::where("state", "active")
        ->get();
        $data = app('App\Http\Controllers\Api\BlastronautApiEndPointController')->activeMissions(new Request());
       
        return Inertia::render('Gamemaster/DevTools/ActiveMiss', [
            'missions' => $data
    
        ]);
    }

} 
<?php 
namespace App\Http\Controllers\Frontend\Gamemaster\DevTools;

use App\Http\Controllers\Controller;
use App\Models\MissionLifecyclePars;
use App\Models\Faction;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;


class AutomaticMissionsPageController extends Controller
{
    
    public function index()
    {   
        $factions = Faction::all();
        $factions_data = array();
        foreach ($factions as $faction){
            $temp = array();
            $temp["id"] =  $faction -> id;
            $temp["name"] =  $faction -> name;
            $temp["voting_time_mins"] =  $faction -> voting_time_minutes;
            $temp["mission_time_mins"] =  $faction -> mission_time_minutes;
            $temp["playerbase_size"] =  $faction -> playerbase_size;
            $factions_data[] = $temp;

        }
        $mission_pars = MissionLifecyclePars::first();
        if (empty($mission_pars)){
            $mlpars = new MissionLifecyclePars;
            $mlpars -> automatic = false;
            $mlpars->save();
            $mission_pars = MissionLifecyclePars::first();
        }

        return Inertia::render('Gamemaster/DevTools/Automiss', [
            'automatic' => $mission_pars,
            'faction_data' => $factions_data,
        ]);
    }

    public function modify(Request $request)
    {    
        DB::beginTransaction();
        $factions = $request->faction_data;
        //var_dump($factions);
        foreach ($factions as $faction){
            $f_db = Faction::find($faction["id"]);
            $f_db -> update([
                "mission_time_minutes" => $faction["mission_time_mins"],
                "voting_time_minutes" => $faction["voting_time_mins"],
                "playerbase_size" => $faction["playerbase_size"],
            ]);
        }
        $mission_pars = MissionLifecyclePars::first();
        $mission_pars -> update([
            "automatic" => $request -> automatic
        ]);
        DB::commit();
        return back();

    }
} 
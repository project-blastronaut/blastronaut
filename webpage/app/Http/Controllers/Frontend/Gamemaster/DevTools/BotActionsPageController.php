<?php 
namespace App\Http\Controllers\Frontend\Gamemaster\DevTools;

use App\Http\Controllers\Controller;
use App\Models\Vote;
use App\Models\Mission;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class BotActionsPageController extends Controller
{
    
    public function index()
    {
        $votes = Vote::where("state", "active")->get();
        $votes->load("missions");
        return Inertia::render('Gamemaster/DevTools/Bot', [
            'votes' => $votes,

        ]);
    }

    public function winningMission(Request $request)
    {
        $request -> validate([
            "vote_id"=>"required",
            "winner_id" =>"required",
        ]);

        //Check vote existance
        $vote = Vote::find($request -> vote_id);
        if (is_null($vote)){
            return response()->json("Invalid vote_id", 400);
        }

        //Verification that vote_id belongs to an active vote.
        if($vote -> state !== "active")
            return response()->json("Vote is not active", 400);


        //Verification that mission belongs to the vote with vote_id.
        $vote_missions = $vote->missions;
        $isValid = false;
        foreach ($vote_missions as $vm){
            if($vm->id == $request -> winner_id){
                $isValid = true;
                break;
            }
        }
        if (! $isValid){
            return response()->json("Mission does not belong to the vote", 400);
        }

        DB::beginTransaction();
        //Redraft lost missions;
        foreach ($vote_missions as $vm){
            $mission = Mission::find($vm->id);
            if ($mission->id !==$request -> winner_id){
                $mission -> update([
                    "state"=>"drafted",
                    "time_limit"=> null
                ]);
            }
        }

        //Add realized resource plan to winning mission. How many resources based on players count.
        
        $mission = Mission::find($request -> winner_id);
        $resources =  $mission->resources;
        $faction = Faction::find($mission->faction_id);
        foreach ($resources as $res){
            $mrr = MissionResourceRequirement::where('mission_id', $mission->id)->where('resource_id', $res->id)->first();
            $mrr->update([
                "specified_amount" => $mrr->amount * $faction->playerbase_size
            ]);
        }
        
        

        $mission = Mission::find($request -> winner_id);
        $mission -> update([
            "state"=>"active",
            "starting_time" => Carbon::now()
        ]);
        
        $vote ->update([
            "state"=> "completed",
            "winning_mission_id"=> $mission -> id,
        ]);

        
        DB::commit();
        return json_encode(array($mission, $vote));
    }

    
} 
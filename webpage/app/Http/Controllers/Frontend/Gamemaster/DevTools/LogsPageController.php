<?php 
namespace App\Http\Controllers\Frontend\Gamemaster\DevTools;

use App\Http\Controllers\Controller;
use App\Models\Mission;
use App\Models\Resource;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Storage;


class LogsPageController extends Controller
{
    
    public function index()
    {
    

        $contents = Storage::disk('local')->get("logs\scheduler.log");
        return Inertia::render('Gamemaster/DevTools/Logs', [
            'contents' => $contents,
        ]);
    }
    
} 
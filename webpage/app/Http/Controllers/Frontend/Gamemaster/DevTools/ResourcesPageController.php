<?php 
namespace App\Http\Controllers\Frontend\Gamemaster\DevTools;

use App\Http\Controllers\Controller;
use App\Models\Mission;
use App\Models\Resource;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Storage;


class ResourcesPageController extends Controller
{
    
    public function index()
    {
        $resources = Resource::all();
        return Inertia::render('Gamemaster/DevTools/Resources', [
            'resources' => $resources,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request -> validate([
            "name" => "required",
            "image" => "required",
            "value" => ["required","gt:0"],
        ]);
        $path = Storage::disk('public')->put('resource_icons', $request->image);
      

        $resource = Resource::create([
            'name' => $request->name,
            'value' => $request->value,
            'image' => $path,
        ]);

        $resources = Resource::all();
        return redirect("/gm/devtools/resources")->with("success", "Resource created successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resource = Resource::find($id);
        if (is_null($resource)){
            return Inertia::render('Gamemaster/DevTools/CreateResource');
        }

        return Inertia::render('Gamemaster/DevTools/EditResource', [
            'resource' => $resource,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $resource = Resource::find($id);
        
        if($request->image != ""){
            $request -> validate([
                "name" => ["required",  "min:1"],
                "value" => ["required", "gt:0"],
                "image" => "image", 
            ]);

            $path = Storage::disk('public')->put('resource_icons', $request->image);
            $resource->update([
                'name' => $request->name,
                'value' => $request->value,
                'image' => $path,
            ]);

        } else {
            $request -> validate([
                "name" => ["required", "min:1"],
                "value" => ["required","gt:0"],
            ]);
            $resource->update([
                'name' => $request->name,
                'value' => $request->value,
            ]);
        }

        return redirect("/gm/devtools/resources")->with("success", "Resource updated successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Resource::destroy($id);
        return redirect("/gm/devtools/resources")->with("success", "Resource deleted successfully!");
    }
} 
<?php 
namespace App\Http\Controllers\Frontend\Gamemaster\DevTools;

use App\Http\Controllers\Controller;
use App\Models\Mission;
use App\Models\Resource;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MissionStatePageController extends Controller
{
    
    public function index()
    {
        $missions = Mission::all();

        return Inertia::render('Gamemaster/DevTools/MissionState', [
            'missions' => $missions,
        ]);
    }
    
} 
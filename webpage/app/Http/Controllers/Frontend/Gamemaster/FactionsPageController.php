<?php 
namespace App\Http\Controllers\Frontend\Gamemaster;
use App\Http\Controllers\Controller;
use App\Models\Faction;
use App\Models\User;
use App\Models\SoldResource;
use App\Models\MissionResourceRequirement;
use App\Models\Mission;
use Inertia\Inertia;

class FactionsPageController extends Controller
{
    public function index()
    {
        $factions = Faction::select("id","name")->get();

        return Inertia::render('Gamemaster/Factions', [
            'factions' => $factions,
        ]);
    }

    public function show($id)
    {   
        $faction = Faction::find($id);
        $collection = collect();
        foreach($faction->players()->get()as $player){
            $points = $player->credoTransactions()->get()->filter(function ($value, $key) use($faction) {
                return $value->faction_id == $faction->id;
            })->sum("quantity");

            $player["credo"]=$points;
            $collection->push($player);
        }
        
        $players=$collection->sortByDesc('credo')->values()->take(10);
        $completedMissions = $faction->missionCompletedTasks;
        $total_points=0;
        foreach( $completedMissions as $completedMission ){
            foreach( $completedMission->requirements as $req ){
                $total_points+=$req->amount * $req->value;                }
        }
        $missions= Mission::select('name', 'id', 'starting_time', 'state','time_limit', 'image', "faction_id","repeatable")
            ->with("resources")
            ->where("faction_id", $id)
            ->where('state', 'failed')
            ->orWhere('state', 'completed')
            ->get();

        foreach($missions as $mission){
            foreach($mission->resources as $resource){
                $sentres = SoldResource::where('mission_id', $mission->id)
                                        ->where('resource_id', $resource->id)
                                        ->get();
                $resource['collected'] = $sentres->sum('quantity');
                $req = MissionResourceRequirement::where('mission_id', $mission->id)
                                ->where('resource_id', $resource->id)
                                ->first();
                $resource['amount'] = $req->amount;
                $resource['specified_amount'] = $req->specified_amount;
            }
            
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $start =  new \DateTime($mission->starting_time);

            $timePassed = $start->diff($now);
            $minutes = $timePassed->days * 24 * 60;
            $minutes += $timePassed->h * 60;
            $minutes += $timePassed->i;
            $mission["time_left"] = $mission->time_limit - $minutes;
        }

        return Inertia::render('Gamemaster/FactionID', [
            'faction' => $faction,
            'missions' => $missions,
            'players' => $players
        ]);
    }
}
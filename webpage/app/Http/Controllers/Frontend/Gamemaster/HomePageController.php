<?php 
namespace App\Http\Controllers\Frontend\Gamemaster;
use App\Http\Controllers\Controller;
use App\Models\Faction;
use App\Models\User;
use App\Models\Mission;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;


class HomePageController extends Controller
{
    public function index()
    {
        $factions = Faction::select('id','logo','name')->withCount('players')->get()->keyBy("name");
        $factions = Faction::select('id','logo','name')->withCount('players')->get()->keyBy("name");
        foreach($factions as $faction){
            $collection = collect();
            foreach($faction->players()->get() as $player){
                $points = $player->credoTransactions()->get()->filter(function ($value, $key) use($faction) {
                    return $value->faction_id == $faction->id;
                })->sum("quantity");

                $player["credo"]=$points;
                $collection->push($player);
            }
            $completedMissions = $faction->missionCompletedTasks;
            $total_points=0;
            foreach( $completedMissions as $completedMission ){
                foreach( $completedMission->requirements as $req ){
                    $total_points+=$req->amount * $req->value;
                }
            }
            $faction->mission_points = ceil($total_points);
            $faction["players"]=$collection->sortByDesc('credo')->values()->take(3);
        }
        return Inertia::render('Gamemaster/Home', [
            'factions' => $factions,
        ]);
    }
}


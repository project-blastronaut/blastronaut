<?php 
namespace App\Http\Controllers\Frontend\Gamemaster;

use App\Http\Controllers\Controller;
use App\Models\Mission;
use App\Models\Resource;
use App\Models\Faction;
use App\Models\MissionResourceRequirement;
use App\Models\SoldResource;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;



class MissionPageController extends Controller
{
    
    public function index()
    {   
        $missions= Mission::select('name', 'id', 'starting_time', 'state','time_limit', 'image', "faction_id","repeatable")
            ->with("resources")
            ->get();

        foreach($missions as $mission){
            foreach($mission->resources as $resource){
                $sentres = SoldResource::where('mission_id', $mission->id)
                                        ->where('resource_id', $resource->id)
                                        ->get();
                $resource['collected'] = $sentres->sum('quantity');
                $req = MissionResourceRequirement::where('mission_id', $mission->id)
                                ->where('resource_id', $resource->id)
                                ->first();
                $resource['amount'] = $req->amount;
                $resource['specified_amount'] = $req->specified_amount;
            }
            
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $start =  new \DateTime($mission->starting_time);

            $timePassed = $start->diff($now);
            $minutes = $timePassed->days * 24 * 60;
            $minutes += $timePassed->h * 60;
            $minutes += $timePassed->i;
            $mission["time_left"] = $mission->time_limit - $minutes;
        }
        $factions = Faction::select("id","name")->get();
        $resources = Resource::select("id", "name")->get();

        return Inertia::render('Gamemaster/Mission', [
            'missions' => $missions,
            'factions' => $factions,
            'resources' => $resources
        ]);
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $request->validate([
            'name' => "required",
            'faction_id' => "required",
            'miss_resources' => 'required',
            'repeatable' => 'required',
        ]);
        
        DB::beginTransaction();

        $mission = Mission::create([
            'name' => $request -> name,
            "faction_id" => $request -> faction_id,
            'description' => $request -> mission_desc,
            'image' => $request -> mission_thumbnail,
            'repeatable' => $request -> repeatable,
        ]);

        foreach ($request -> miss_resources as $res){
            MissionResourceRequirement::create([
                'mission_id' => $mission -> id,
                "amount" => $res[1],
                'resource_id' => $res[0],
            ]);
        }
        DB::commit();
        return redirect("gm/missions")->with('success','New mission added!');

    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Mission::destroy($id);
        return redirect("gm/missions")->with('success','Mission deleted successfully');
    }

    public function show($id)
    {   
        $mission = Mission::find($id);

        $missions->load("requirements");

        $factions = Faction::select("id","name")->get();
        $resources = Resource::select("id", "name")->get();

        return Inertia::render('MissionID', [
            'mission' => $mission,
            'factions' => $factions,
            'resources' => $resources
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $request->validate([
            'name' => "required",
            'faction_id' => "required",
            'miss_resources' => 'required',
            "repeatable" => "required",
        ]);

        $mission = Mission::find($id);
        if ($mission->state != "drafted"){
            
            throw \Illuminate\Validation\ValidationException::withMessages([
                'state' => ['mission must be in drafting state'],
            ]);
        } 
        DB::beginTransaction();

        $mission->update([
            'name' =>$request->name,
            'image' =>$request->mission_thumbnail,
            'faction_id' =>$request->faction_id,
            'description' =>$request->mission_desc,
            'repeatable' => $request->repeatable,
        ]);

        $newReqs=array();

        forEach($request->miss_resources as $req){
            $newReqs[$req[0]] = ['amount'=>$req[1]];
        }
    
        $res = $mission->resources()->sync($newReqs);
       
        DB::commit();

        return redirect("gm/missions")->with("success", "Mission updated");
    }

    
} 
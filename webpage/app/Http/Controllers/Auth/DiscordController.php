<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Inertia\Inertia;

use Auth;

class DiscordController extends Controller
{
    /**
     * Redirect the user to the Discord authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('discord')
        ->scopes(['identify', 'email','guilds'])
        ->redirect();
    }

    /**
     * Obtain the user information from Discord.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver("discord")->user();
        } catch (ClientException $exception) {
            return response()->json(['error' => 'Invalid credentials provided.'], 422);
        }

        $userCreated = User::updateOrCreate(
            [
                'discord_id' => (int) $user->id,
            ],
            [
                'image' => $user->avatar,
                'tag' => $user->nickname,
                'email' => $user->email,
                'email_verified_at' => now(),
            ]
        );
   
        $userCreated->providers()->updateOrCreate(
            [
                'provider' => 'discord',
                'provider_id' => $user->getId(),
            ],
            [
                'avatar' => $user->getAvatar()
            ]
        );
        if ($userCreated->is_gm==false){
            return Inertia::render('Guest/ErrorPage', [
                'reason'=>"Your account does not have admin rights."
            ]);   
        }
        $token = $userCreated->createToken('token-name')->plainTextToken;
        Auth::login($userCreated);
        return redirect("/gm");
       
    }
}
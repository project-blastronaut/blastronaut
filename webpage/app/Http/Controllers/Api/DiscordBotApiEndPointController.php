<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Faction;
use App\Models\Mission;
use App\Models\CredoTransaction;
use App\Models\User;
use App\Models\MissionResourceRequirement;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class DiscordBotApiEndPointController extends Controller
{   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function WinningMission(Request $request)
    {   
        Storage::disk('local')->append('logs/scheduler.log', now().' Trying to send winner of the vote');

        $validator = Validator::make($request->all(),[
            "vote_id"=>"required",
            "winner_id" =>"required",
            "hash" => "required",
        ]);
        $validator->after(function ($validator) {
            foreach($validator->errors()->all() as $error) {
                Storage::disk('local')->append('logs/scheduler.log', now().' Validation error: '. $error);
            };
        });
        $validator->validate();

        //Check vote existance
        $vote = Vote::find($request -> vote_id);
        if (is_null($vote)){
            Storage::disk('local')->append('logs/scheduler.log', now().' Invalid vote id');
            return response()->json("Invalid vote_id", 400);
        }

        //Check hash
        $data = json_encode(array(
            "vote_id" => intval($request -> vote_id),
            "winner_id" =>  intval($request -> winner_id),
            "secret" => '68ad3d31c0f17d553762434ed0bb09caf86d9b40bc71298e04903d116117ea33'
            )
        );
        $digest = hash('sha256', $data);
        if($digest != $request->hash){
            Storage::disk('local')->append('logs/scheduler.log', now().' Hash mismatch');
            return response()->json("Hash mismatch", 400);
        }

        //Verification that vote_id belongs to an active vote.
        if($vote -> state !== "active"){
            Storage::disk('local')->append('logs/scheduler.log', now()."Vote is not active");
            return response()->json("Vote is not active", 400);
        }


        
        //Verification that mission belongs to the vote with vote_id.
        $vote_missions = $vote->missions;
        $isValid = false;
        foreach ($vote_missions as $vm){
            if($vm->id == $request -> winner_id){
                $isValid = true;
                break;
            }
        }
        if (! $isValid){
            return response()->json("Mission does not belong to the vote", 400);
        }

        DB::beginTransaction();
        //Redraft lost missions;
        foreach ($vote_missions as $vm){
            $mission = Mission::find($vm->id);
            if ($mission->id !==$request -> winner_id){
                $mission -> update([
                    "state"=>"drafted",
                    "time_limit"=> null
                ]);
            }
        }

        $mission = Mission::find($request -> winner_id);
        $resources =  $mission->resources;
        $faction = Faction::find($mission->faction_id);
        foreach ($resources as $res){
            $mrr = MissionResourceRequirement::where('mission_id', $mission->id)->where('resource_id', $res->id)->first();
            $mrr->update([
                "specified_amount" => $mrr->amount * $faction->playerbase_size
            ]);
        }
        
        $mission = Mission::find($request -> winner_id);
        $mission -> update([
            "state"=>"active",
            "starting_time" => Carbon::now()
        ]);
        
        $vote ->update([
            "state"=> "completed",
            "winning_mission_id"=> $mission -> id,
        ]);

        Storage::disk('local')->append('logs/scheduler.log', now().' Winner selected');
        DB::commit();
        return json_encode(array($mission, $vote));
    }


    public function credoTransaction(Request $request)
    {   
        Storage::disk('local')->append('logs/scheduler.log', now().' Trying to send credo');

        $validator = Validator::make($request->all(),[
            "player_id"=>"required",
            "amount" => [
                "required",
                "gt:0",
            ],
            "hash" => "required"
        ]);
        $validator->after(function ($validator) {
            foreach($validator->errors()->all() as $error) {
                Storage::disk('local')->append('logs/scheduler.log', now().' Validation error: '. $error);
            };
        });
        $validator->validate();

         //Check hash
        $data = json_encode(array(
            "player_id" => $request -> player_id,
            "amount" =>  intval($request -> amount),
            "secret" => "68ad3d31c0f17d553762434ed0bb09caf86d9b40bc71298e04903d116117ea33")
        );
        
        $digest = hash('sha256', $data);
        if($digest != $request->hash){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Hash mismatch');
            return response()->json("Hash mismatch", 400);
        }

        //Check player existance
        $player = User::where("discord_id", $request -> player_id)->first();
        if (is_null($player)){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Can not find player with given id');
            return response()->json("Can not find player with given id", 404);
        }
        $ct = CredoTransaction::create([
            'faction_id' => $player->faction_id,
            'player_id' => $player->id,
            "quantity" => $request->amount,
            "reason" => 'social',
        ]);

        Storage::disk('local')->append('logs/scheduler.log', now().' Success: Transaction completed');
        return json_encode($ct);
    }


    public function updateUserInfo(Request $request)
    {   
        Storage::disk('local')->append('logs/scheduler.log',  now().' Trying to update user information.');
        $validator = Validator::make($request->all(),[
            "discord_id"=> "required",
            "username"=>"required",
            "hash" => "required",
        ]);

        $validator->after(function ($validator) {
            foreach($validator->errors()->all() as $error) {
                Storage::disk('local')->append('logs/scheduler.log', now().' Validation error: '. $error);
            };
        });
        $validator->validate();
        

        //Check hash
        $data = array();
        foreach($request->except('hash') as $key => $value){
            if ($key == 'faction_id'){
                $data['faction_id'] = is_null($value) ? null : intval($value);
            } else if($key == "is_gm"){
                $data['is_gm'] = intval($value);
            } else {
                $data[$key] = $value;
            }
        }

        $data['secret']="68ad3d31c0f17d553762434ed0bb09caf86d9b40bc71298e04903d116117ea33";

        $data = json_encode($data, JSON_UNESCAPED_SLASHES);
        $digest = hash('sha256', $data);

        if($digest != $request->hash){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Hash mismatch');
            return response()->json("Hash mismatch", 400);
        }
        
        DB::beginTransaction();

        //Check player existance
        $user = User::firstOrCreate([
            "discord_id"=> $request -> discord_id
        ]);

       
        $user->name = $request->username;
        $user->image = $request->avatar_url;

        if ($request->has('faction_id')) {
            $faction = Faction::find($request->faction_id);
            if (is_null($faction) && !is_null($request->faction_id)){
                Storage::disk('local')->append('logs/scheduler.log', now().' Error: Can not find faction with given id '. $request->faction_id);
                return response()->json("Can not find faction with given id", 404);
            }
            $user->faction_id = $request->faction_id;
        }

        if ($request->has('tag')) {
            $user->tag = $request->tag;
        }

        if ($request->has('is_gm')) {
            $user->is_gm = intval($request->is_gm);
        }
        $user->save();
        DB::commit();
        Storage::disk('local')->append('logs/scheduler.log', now().' Player information added');
        return json_encode($user);
    }

    public function getPlayerPoints(Request $request, $id)
    {   
        $user = User::where("discord_id", $id)->select('id', 'discord_id', 'faction_id')->first();
        $points = $user->credoTransactions()->where("faction_id", $user->faction_id)->get()->sum('quantity');
        $user["credo"] = $points;
        return json_encode($user);
    }
}

    

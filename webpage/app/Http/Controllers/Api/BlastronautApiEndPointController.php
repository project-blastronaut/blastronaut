<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Faction;
use App\Models\Mission;
use App\Models\Resource;
use App\Models\User;
use App\Models\MissionResourceRequirement;
use App\Models\SoldResource;
use App\Models\CredoTransaction;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class BlastronautApiEndPointController extends Controller
{
    public function activeMissions(Request $request)
    {  
          
        $missions= Mission::where("state", "active")
            ->select('name', 'id', 'starting_time', 'time_limit', 'image', "faction_id")
            ->with("resources")
            ->get();

        foreach($missions as $mission){
            foreach($mission->resources as $resource){
                $sentres = SoldResource::where('mission_id', $mission->id)
                                        ->where('resource_id', $resource->id)
                                        ->get();
                $resource['collected'] = $sentres->sum('quantity');
                $req = MissionResourceRequirement::where('mission_id', $mission->id)
                                ->where('resource_id', $resource->id)
                                ->first();
                $resource['amount'] = $req->amount;
                $resource['specified_amount'] = intval($req->specified_amount);
            }
            
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $start =  new \DateTime($mission->starting_time);

            $timePassed = $start->diff($now);
            $minutes = $timePassed->days * 24 * 60;
            $minutes += $timePassed->h * 60;
            $minutes += $timePassed->i;
            $mission["time_left"] = $mission->time_limit - $minutes;
        }
        return $missions;
    }


    public function sendResources(Request $request)
    {  
        Storage::disk('local')->append('logs/scheduler.log', now().' Trying to send resources.');

        $validator = Validator::make($request->all(),[
            'mission_id' => "required",
            //'player_tag' => "required",
            "quantity" => "required|gt:0",
            "resource_id" => "required",
            "hash" => "required",
        ]);


        $validator->after(function ($validator) {
            foreach($validator->errors()->all() as $error) {
                Storage::disk('local')->append('logs/scheduler.log', now().' Validation error: '. $error);
            };
        });
        $validator->validate();

        $player= ($request->has('player_tag') && !empty($request->player_tag)) ? User::where("tag", $request->player_tag)->first() : null;
     

        $mission = Mission::find($request->mission_id);
        if (!$mission){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Mission with given ID does not exist');
            return response()->json("Mission with given ID does not exist", 404);
        }

        if ($mission->state!="active"){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Mission with given ID is not active');
            return response()->json("Mission with given ID is not active", 400);
        }

        $resource = Resource::find($request->resource_id);
        if (!$resource){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Resource with given ID does not exist');
            return response()->json("Resource with given ID does not exist", 404);
        }

        $requirement =  MissionResourceRequirement::where('mission_id',$mission->id)->where('resource_id', $request->resource_id);
        if (!$requirement){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Mission does not require this resource');
            return response()->json("Given mission does not require this resource", 400);
        }
        
        if ($request->has('player_tag')){
            $data = $request->mission_id . $request->player_tag . $request->quantity . $request->resource_id . "b62a013c29802616fb9460e5b7dd5bf5ab725dd01a7c7c418680c92911fbc7d6";
        } else {
            $data = $request->mission_id . $request->quantity . $request->resource_id . "b62a013c29802616fb9460e5b7dd5bf5ab725dd01a7c7c418680c92911fbc7d6";
        }

        $digest = hash('sha256', $data);
        if($digest != $request->hash){
            Storage::disk('local')->append('logs/scheduler.log', now().' Error: Hash mismatch');
            return response()->json("Hash mismatch", 400);
        }

        $required =  $requirement->first()->specified_amount;
        $collected = SoldResource::where('mission_id',$mission->id)->where('resource_id', $request->resource_id)->sum('quantity');
     
        if ((int)$required < $collected + $request->quantity){
            Storage::disk('local')->append('logs/scheduler.log',
                now().' Info: More resources sent than were required. Extra resources were accepted, but no metagame credit was awarded. Required: '.$required." Already collected: ". $collected." You tried to send ".$request->quantity);
                $request->quantity = (int)$required - $collected;
            }

        DB::beginTransaction();
        if ((int)$required == $collected + $request->quantity){
            $allCollected=True;
            foreach($mission->resources as $resource){
                $sentres = SoldResource::where('mission_id', $mission->id)
                                        ->where('resource_id', $resource->id)
                                        ->get();
                
                $req = MissionResourceRequirement::where('mission_id', $mission->id)
                                ->where('resource_id', $resource->id)
                                ->first();
                if ($sentres->sum('quantity') < intval($req->specified_amount)){
                    $allCollected=false;
                    break;
                }
            }
            if ($allCollected){
                $mission->update([
                    "state" => "completed",
                ]);

                if ($mission->repeatable){
            
                    $newMission = Mission::create([
                        "name" => $mission->name,
                        "repeatable" => true,
                        "image" => $mission->image,
                        "faction_id" => $mission->faction_id
                    ]);
                    
                    foreach($mission->resources as $res)
                    {   
                        $mrr = MissionResourceRequirement::where('mission_id', $mission->id)->where('resource_id', $res->id)->first();
                        
                        MissionResourceRequirement::create([
                            'mission_id' => $newMission -> id,
                            "amount" => $mrr->amount,
                            'resource_id' => $res->id,
                        ]);
                    }
                }
            }
            
        }
        

        $discord_id="unknown";

        if ($player != null && $player->faction_id == $mission->faction_id){
            CredoTransaction::create([
                'faction_id' => $mission->faction_id,
                'player_id' =>$player->id,
                "quantity" => ceil($resource->value * intval($request->quantity)),
                "reason" => 'resource',
            ]);

            $sr = SoldResource::create([
                "mission_id" => $request->mission_id,
                'player_id' =>$player->id,
                "quantity" =>$request->quantity,
                "resource_id" => $request->resource_id,
            ]);
            $discord_id = $player->discord_id;
        } else {
            $sr = SoldResource::create([
                "mission_id" => $request->mission_id,
                "quantity" =>$request->quantity,
                "resource_id" => $request->resource_id,
            ]);
        }

        $data_to_discord = array();
        if($request->quantity>0){
            $data_to_discord["command_name"] = "resource_sent";
            $data_to_discord["player_id"] = $discord_id;
            $data_to_discord["quantity"] = $request->quantity;
            $data_to_discord["credits"] = ceil($resource->value * intval($request->quantity));
            $data_to_discord["resource_name"] = $resource->name;
            $data_to_discord["faction_id"] = $mission->faction_id;
            $data_to_discord["mission_name"] = $mission->name;
        }
        
        $this->sendVoteCommand('https://discord.com/api/webhooks/895934806888685608/5d8H7m_4RmyjsOnUJhMKa-3AQihz9iAtsMtlND_jhNQ9sIbVKXgmhuJWHhbYCTfb2HSo', json_encode($data_to_discord));
        DB::commit();

        if ($player==null){
            Storage::disk('local')->append('logs/scheduler.log', now().' Success! Anonymous resources sent.');
            return response()->json($sr, 206);
        }
        Storage::disk('local')->append('logs/scheduler.log', now().' Success! Resources sent.');
        return response()->json($sr, 200);

    }
    
    private function sendVoteCommand($endpoint, $data){
        $data = json_encode(array("avatar_url"=> "", "content"=> $data));
        $headers= array('Accept: application/json','Content-Type: application/json'); 

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $endpoint);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return;
    }
}

    

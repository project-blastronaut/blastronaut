<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Mission;
use App\Models\MissionResourceRequirement;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class deleteTimedOutMissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:missions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info(now() ." Timed out mission cleaner started");

        $missions = Mission::where("state", "active")->get();
        DB::beginTransaction();

        foreach ($missions as $mission){
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $started = new \DateTime($mission->starting_time);
            $timePassed = $started->diff($now);
            $minutes = $timePassed->days * 24 * 60;
            $minutes += $timePassed->h * 60;
            $minutes += $timePassed->i;
            if ($minutes > $mission -> time_limit+1){
                $this->info(now()." Resolving timed out mission. Time passed:" . $minutes ." Critical point: " . $mission -> time_limit + 1 ."minutes". PHP_EOL);
                $mission->update([
                    "state" => "failed",
                ]);
                if ($mission->repeatable){
    
                    $newMission = Mission::create([
                        "name" => $mission->name,
                        "repeatable" => true,
                        "image" => $mission->image,
                        "faction_id" => $mission->faction_id
                    ]);
                   

                    foreach($mission->resources as $resource)
                    {   
                        $mrr = MissionResourceRequirement::where('mission_id', $mission->id)->where('resource_id', $resource->id)->first();
                        
                        MissionResourceRequirement::create([
                            'mission_id' => $newMission -> id,
                            "amount" => $mrr->amount,
                            'resource_id' => $resource->id,
                        ]);
                    }
                }    
            }
        };

        $this->info(now()." Timed out mission cleaner finished");
        DB::commit();
        return Command::SUCCESS;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Faction;
use App\Models\Vote;
use App\Models\Mission;
use Illuminate\Support\Facades\DB;


class HandleVotes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'progress:voting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function shouldDispatchNewVote($faction_id){
      
        $active_votes = Vote::where("state", "active")->where("faction_id", $faction_id)->get();
        $active_missions = Mission::where("state","active")->where("faction_id", $faction_id)->get();

        return $active_missions->isEmpty() && $active_votes->isEmpty();      
    }

    private function getMissions($faction_id, $mission_amount){
        $missions = Mission::where('faction_id', $faction_id)
        ->where("state", "drafted")
        ->inRandomOrder()
        ->limit($mission_amount);

        if ($missions->get()->count() < $mission_amount){
            throw new \BadMethodCallException("Faction: ". $faction_id . " Requsted ". $mission_amount ." missions. However only ". $missions->get()->count() ." drafted missions in the mission pool");
        }
        return $missions->get();
    }

    private function constructMissionVote($faction, $mission_amount){
        $missions = $this->getMissions($faction->id, $mission_amount);

        $vote = Vote::create([
            "title" => "What shall we construct next?", 
            "faction_id" => $faction->id,
            "voting_time_minutes" => $faction->voting_time_minutes,
            "state" => 'active',
            "image" => $faction->logo
        ]);

        foreach($missions as $mission){
            $mission->update([
                "state" => "voting",
                "time_limit" => $faction->mission_time_minutes
            ]);
            $vote->missions()->attach($mission->id);
        }
        return $vote;
    }

    /**
     * preparing mission data for vote
     */
    private function constructVoteCommandContent($vote ){
        $data = array();
        foreach ($vote->missions as $mission){
            $reqs = $mission->requirements;
            $temp = array();
            foreach ($reqs as $req) {
                $temp[$req -> resource -> name] = $req -> amount;
            }
            $vote_miss = array();
            $vote_miss["id"] = $mission->id;
            $vote_miss["name"] = $mission->name;
            $vote_miss["time_limit"] = $mission->time_limit;
            $vote_miss["resources"] = $temp;
            $vote_miss["image_url"] = $mission->image;
            $data[] =  $vote_miss;
        };

        $content = json_encode(array(
            "command_name" => "create-vote",
            "vote_id" => $vote->id,
            "vote_faction" => $vote->faction_id,
            "vote_duration" => $vote->voting_time_minutes, 
            "vote_title" => $vote->title,
            "vote_thumbnail_url" => $vote->image,
            "vote_choices" => $data
        ));
        return $content;
    }


    private function sendVoteCommand($endpoint, $data){
        $data = json_encode(array("avatar_url"=> "", "content"=> $data));
        $headers= array('Accept: application/json','Content-Type: application/json'); 

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $endpoint);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $factions = Faction::all();
        foreach ($factions as $faction){
            try {
                if ($this -> shouldDispatchNewVote($faction -> id)){
                    $this -> info(now(). " Trying to start a new vote for ". $faction->id);
                    DB::beginTransaction();
                    $vote = $this -> constructMissionVote($faction, 3);
                    $data = $this -> constructVoteCommandContent($vote);
                    $this -> sendVoteCommand('https://discord.com/api/webhooks/895934806888685608/5d8H7m_4RmyjsOnUJhMKa-3AQihz9iAtsMtlND_jhNQ9sIbVKXgmhuJWHhbYCTfb2HSo', $data);
                    DB::commit();   
                };
            } catch (\BadMethodCallException $e) {
                DB::rollback();   
                $this -> error(now()." ". $e->getMessage());
            } catch (\Exception $e){
                DB::rollback();   
                $this -> error(now()." ". $e->getMessage());
            }
        }
        return Command::SUCCESS;
    }
}

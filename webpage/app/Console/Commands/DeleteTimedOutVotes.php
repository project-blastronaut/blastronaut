<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Vote;
use App\Models\Mission;
use Illuminate\Support\Facades\DB;



class DeleteTimedOutVotes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:votes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $this->info(now(). " Timed out vote cleaner started");
        $votings = Vote::where("state", "active")->get();
        foreach ($votings as $voting){
            DB::beginTransaction();
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $timePassed = $voting->created_at->diff($now);

            $minutes = $timePassed->days * 24 * 60;
            $minutes += $timePassed->h * 60;
            $minutes += $timePassed->i;

            if ($minutes > $voting -> voting_time_minutes+5){
                $this->info(now(). " Resolving timed out vote. Time passed:" . $minutes ." Critical point: " . ($voting -> voting_time_minutes + 5) ."minutes". PHP_EOL);
                $voting->update(["state"=>"failed"]);
               
                foreach($voting -> missions as $vote_mission){
                    $vote_mission -> update(["state" => "drafted"]);
                }
            }
            DB::commit();

        };
        $this->info(now()." Timed out vote cleaner finished");
        return Command::SUCCESS;
    }
}

<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\MissionLifecyclePars;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {   
    
      
        $schedule->command('clean:votes')
            ->everyMinute()
            ->appendOutputTo(storage_path('logs/scheduler.log'));

        $schedule->command('clean:missions')
            ->everyMinute()
            ->appendOutputTo(storage_path('logs/scheduler.log'));

    
        $schedule->command('logcleaner:run')->daily()->at('01:00');

        
        $schedule->command('progress:voting')
            ->everyMinute()
            ->when(function(){
                $mission_pars = MissionLifecyclePars::first();
                if (empty($mission_pars)){
                    $mlpars = new MissionLifecyclePars;
                    $mlpars -> automatic = false;
                    $mlpars->save();
                    $mission_pars = MissionLifecyclePars::first();
                }
                return $mission_pars -> automatic === 1;
            })
            ->appendOutputTo(storage_path('logs/scheduler.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

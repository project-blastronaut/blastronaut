<?php

namespace App\Jobs;
use DateInterval;
use Illuminate\Bus\Queueable;
use App\Models\Mission;
use App\Models\Vote;
use App\Models\VoteMissions;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\MissionLifecyclePars;
use Illuminate\Support\Facades\DB;

class Cleaning implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {   
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle()
    {   
        #Removes all timed out votes and missions.
        
        #Removing votes
        DB::beginTransaction();
        $votings = Vote::where("state", "active")->get();
        foreach ($votings as $voting){
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $timePassed = $voting->created_at->diff($now);
            $minutes = $timePassed->days * 24 * 60;
            $minutes += $timePassed->h * 60;
            $minutes += $timePassed->i;
            if ($minutes > $voting -> voting_time_minutes+5){
                echo("Resolving timed out vote. Time passed:" . $minutes ." Critical point: " . $voting -> voting_time_minutes + 5 ."minutes". PHP_EOL);
                $voting->update(["state"=>"failed"]);
                foreach($voting -> missions as $vote_mission){
                    $mission = Mission::find($vote_mission->mission_id);
                    var_dump($mission);
                    $mission -> update(["state" => "drafted"]);
                }
            }
        };

       
        $missions = Mission::where("state", "active")->get();

        foreach ($missions as $mission){
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $timePassed = $mission->created_at->diff($now);
            $minutes = $timePassed->days * 24 * 60;
            $minutes += $timePassed->h * 60;
            $minutes += $timePassed->i;
            if ($minutes > $mission -> time_limit+1){
                //Check if enough resources have been collected should be implemented. Current implementation relies on fact that when enough resources are sent then mission is 
                //completed and not active.
                echo("Resolving timed out mission. Time passed:" . $minutes ." Critical point: " . $mission -> time_limit + 1 ."minutes". PHP_EOL);
                $mission->update([
                    "state" => "failed",
            ]);
            }
        };


        DB::commit();

    }
}

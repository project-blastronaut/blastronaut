<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use App\Models\MissionLifecyclePars;
use Throwable;



class MissionLifecycle implements ShouldQueue#, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {   
    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle()
    {   
       
        var_dump("MODE: ". $isAutomaticMode);
        if($isAutomaticMode===1){
            Bus::chain([
                new Cleaning, #Cleaning timed out missions and votes
                new HandleVoteSending, #According to current situation either waits, sends a vote or starts a new mission
            ])->catch(function (Throwable $e) {
                var_dump($e->getMessage());
            })->dispatch();
        }else{
            Bus::chain([
                new Cleaning, #Cleaning timed out missions and votes
            ])->catch(function (Throwable $e) {
                var_dump($e->getMessage());
            })->dispatch();
        }
        sleep(10);
        MissionLifecycle::dispatch();

    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Models\Mission;
use App\Models\Vote;
use App\Models\VoteMissions;
use App\Models\FailedJob;
use App\Models\Faction;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\MissionLifecyclePars;
use Illuminate\Support\Facades\DB;
use Throwable;

class CreateVote implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $faction_id;
    private $mission_amount;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($faction_id, $mission_amount)
    {   
      
        $this -> faction_id = $faction_id;   
        $this -> mission_amount = $mission_amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle()
    {   
        DB::beginTransaction();
        $missions = Mission::where('faction_id', $this -> faction_id)
        ->where("state", "drafted")
        //->with('mission_resource_requirement')
        ->inRandomOrder()
        ->limit($this-> mission_amount);
    
        if ($missions->get()->count() < $this-> mission_amount){
            throw new \BadMethodCallException("Faction: ". $this -> faction_id . " Requsted ". $this-> mission_amount ." missions. However only ". $missions->get()->count() ." drafted missions in the mission pool");
        }

        $faction = Faction::find($this -> faction_id);

        $vote = new Vote;
        $vote -> faction_id = $this -> faction_id;
        $vote -> title= "What shall we do next?";
        $vote -> voting_time_minutes = $faction->voting_time_minutes;
        $vote -> state = "active";
        $vote -> save();
        
        $voting_missions = array();
        foreach ($missions->get() as $mission){
            //adding mission to the vote
            $voteMission = new VoteMissions;
            $voteMission -> vote_id = $vote -> id;
            $voteMission -> mission_id = $mission-> id;
            $voteMission -> save();

            //updating state of the mission
            $mission -> update(["state" => "voting"]);
            $mission -> update(["time_limit" => $faction->mission_time_minutes]);
            
            //preparing mission data for vote
            $reqs = $mission->requirements;
            $temp = array();
            foreach ($reqs as $req) {
                $temp[$req -> resource -> name] = $req -> amount;
            }
            $vote_miss = array();
            $vote_miss["id"] = $mission->id;
            $vote_miss["name"] = $mission->name;
            $vote_miss["time_limit"] = $faction -> mission_time_minutes;
            $vote_miss["resources"] = $temp;
            $vote_miss["image_url"] = $mission -> image;

            $voting_missions[] =  $vote_miss;
        }
        
        //preparing the vote content
        $content = json_encode(array(
            "command_name" => "create-vote",
            "vote_id" => $vote -> id,
            "vote_faction" => $this -> faction_id,
            "vote_duration" => $vote -> voting_time_minutes, 
            "vote_title" => $vote -> title,
            "vote_thumbnail_url" => $faction -> logo,
            "vote_choices" => $voting_missions
        ));

        //sending the vote
        $data = json_encode(array("avatar_url"=> "", "content"=> $content));
        $headers= array('Accept: application/json','Content-Type: application/json'); 

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, 'https://discord.com/api/webhooks/895934806888685608/5d8H7m_4RmyjsOnUJhMKa-3AQihz9iAtsMtlND_jhNQ9sIbVKXgmhuJWHhbYCTfb2HSo');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        var_dump($response);
        curl_close($curl);

        DB::commit();   
    }

    public function failed(Throwable $e)
    {   
        var_dump($e->getMessage());
        FailedJob::truncate();
        return ;
    }
}

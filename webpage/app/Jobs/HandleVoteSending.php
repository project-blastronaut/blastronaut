<?php

namespace App\Jobs;
use DateInterval;
use Illuminate\Bus\Queueable;
use App\Models\Mission;
use App\Models\Vote;
use App\Models\Faction;
use App\Models\VoteMissions;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\MissionLifecyclePars;
use App\Jobs\CreateVote;
use Illuminate\Support\Facades\DB;

class HandleVoteSending implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {   
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    
    public function handle()
    {   
        #Starting new votes if no other vote or mission exists
        $factions = Faction::all();
        foreach ($factions as $faction){

            $active_votes = Vote::where("state","active")->where("faction_id", $faction -> id)->get();
            $active_missions = Mission::where("state","active")->where("faction_id", $faction -> id)->get();

            if ($active_missions->isEmpty() && $active_votes->isEmpty()){
                CreateVote::dispatch($faction -> id, 3);
            }
        }
    }
}

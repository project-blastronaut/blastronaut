const defaultTheme = require('tailwindcss/defaultTheme');


// tailwind.config.js
module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.js',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            minWidth: {
                '44': '11rem',
              },
            minHeigth: {
                '44': '11rem',
            },
        },
        inset: {
            '0': 0,
            // ...
            '64': '16rem',
            '1/5': '20%',
        },
        backgroundColor: theme => ({
       ...theme('colors'),
       'main': '#212E38',
       'precorics': '#42A46D',
       'vector': '#7F52C8',
       'operative': '#FFAD33',
       'backestBack':'#1D2529',
       'SteamRed':'#891E2B'
        }),
        fontFamily: {
            'quest': 'Questrial ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"'
        }
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms')],
};

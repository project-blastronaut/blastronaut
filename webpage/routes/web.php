<?php


use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Old homepage
Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});*/
Route::group([
], function(){
    Route::get('/', [\App\Http\Controllers\Frontend\Guest\HomePageController::class, 'index']);
    Route::get('/login', [\App\Http\Controllers\Frontend\Guest\HomePageController::class, 'index']);
    Route::resource('factions', \App\Http\Controllers\Frontend\Guest\FactionsPageController::class);
    Route::get('presskit', function(){
        return Inertia::render('Guest/Presskit');
    });
    Route::get('/missions', [\App\Http\Controllers\Frontend\Guest\MissionPageController::class, 'index']);
    Route::get('storage/{folder}/{file}', function ($folder, $file)
    {
        $path = storage_path('app/public/' . $folder . '/' . $file );
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    });
});


Route::group([
    'prefix' => 'gm',
    'middleware' => "is_gm",
    'as' => 'gm.'
    ], function(){
    Route::get('', [\App\Http\Controllers\Frontend\Gamemaster\HomePageController::class, 'index']);
  
    Route::redirect('devtools', '/gm/devtools/dispatch');
    Route::resource('factions', \App\Http\Controllers\Frontend\Gamemaster\FactionsPageController::class);
    Route::get('devtools/dispatch', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\MissionDispatchPageController::class, 'index']);
    Route::get('devtools/automiss', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\AutomaticMissionsPageController::class, 'index']);
    Route::post('devtools/automiss', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\AutomaticMissionsPageController::class, 'modify']);
    Route::get('devtools/sendres', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\SendResourcePageController::class, 'index']);
    Route::post('devtools/sendres', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\SendResourcePageController::class, 'store']);
    Route::get('devtools/missionstatus', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\MissionStatePageController::class, 'index']);
    Route::get('devtools/activemiss', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\ActiveMissionsPageController::class, 'index']);
    Route::get('devtools/logs', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\LogsPageController::class, 'index']);
    Route::get('devtools/bot', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\BotActionsPageController::class, 'index']);
    Route::post('devtools/bot', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\BotActionsPageController::class, 'winningMission']);
    Route::post('devtools/resources/{id}', [\App\Http\Controllers\Frontend\Gamemaster\DevTools\ResourcesPageController::class, "update"]);
    Route::get('presskit', function(){
        return Inertia::render('Gamemaster/Presskit');
    });
    Route::resource('devtools/resources', \App\Http\Controllers\Frontend\Gamemaster\DevTools\ResourcesPageController::class)->except("update");
    Route::resource('missions', \App\Http\Controllers\Frontend\Gamemaster\MissionPageController::class);
});


require __DIR__.'/auth.php';

<?php
use App\Http\Controllers\Api\MissionController;
use App\Http\Controllers\Api\FactionController;
use App\Http\Controllers\Api\PlayerController;
use App\Http\Controllers\Api\SoldResourceController;
use App\Http\Controllers\Api\ResourceController;
use App\Http\Controllers\Api\DiscordBotApiEndPointController;
use App\Http\Controllers\Api\BlastronautApiEndPointController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/discordBot/winningMission', [DiscordBotApiEndPointController::class, "WinningMission"]);
Route::post('/discordBot/credo', [DiscordBotApiEndPointController::class, "credoTransaction"]);
Route::post('/discordBot/updatePlayer', [DiscordBotApiEndPointController::class, "updateUserInfo"]);
Route::get('/discordBot/credo/{id}', [DiscordBotApiEndPointController::class, "getPlayerPoints"]);
Route::get('/activemissions', [BlastronautApiEndPointController::class, "activeMissions"]);
Route::post('/sendresource', [BlastronautApiEndPointController::class, "sendResources"]);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\DiscordController;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;



Route::get('/login/discord/callback', [DiscordController::class, 'handleProviderCallback'])
                ->middleware('guest');


Route::get('/login/discord', [DiscordController::class, 'redirectToProvider'])
                ->middleware('guest')
                ->name('login');

Route::get('/login', function (){
    return redirect('/login/discord');
});

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');

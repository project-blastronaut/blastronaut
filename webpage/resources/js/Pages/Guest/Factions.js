import precoricsIcon from '../../../pictures/precorics.png'
import operativeIcon from '../../../pictures/operativeunited.png'
import vectorIcon from '../../../pictures/vectoraccelerate.png'
import React from 'react';
import { Head, usePage } from '@inertiajs/inertia-react';
import Guest from '@/Layouts/Guest';

export default function Factions(props) {

    return (
        <Guest>
            <Head title="Factions" />
            <div className="flex items-center">
                <div className="flex flex-col items-center space-y-20">
                    <h1 className="text-white text-5xl	sm:text-9xl mt-20">Factions</h1>
                    <div className="sm:flex flex-row items-center space-y-10">
                        <a href="/factions/3">
                            <img className="object-contain" src={precoricsIcon}></img>
                        </a>
                        <a href="/factions/2">
                            <img className="object-contain" src={vectorIcon}></img>
                        </a>
                        <a href="/factions/1">
                            <img className="object-contain" src={operativeIcon}></img>
                        </a>
                    </div>
                </div>
            </div>
        </Guest>
    );
}
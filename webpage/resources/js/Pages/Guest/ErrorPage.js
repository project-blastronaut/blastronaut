import precoricsIcon from '../../../pictures/precorics.png'
import operativeIcon from '../../../pictures/operativeunited.png'
import vectorIcon from '../../../pictures/vectoraccelerate.png'
import React from 'react';
import { Head, usePage } from '@inertiajs/inertia-react';
import Guest from '@/Layouts/Guest';

export default function ErrorPage({reason}) {
    return (
        <Guest>
            <div className="flex items-center">
                <div className="flex flex-col justify-center items-center space-y-20 w-full">
                    <h1 className="text-white text-xl mt-20">Sorry! An error occured!</h1>
                    <div className="flex flex-row items-center space-y-10">
                        <h2 className="text-white text-md" >{reason}</h2>
                    </div>
                </div>
            </div>
        </Guest>
    );
}
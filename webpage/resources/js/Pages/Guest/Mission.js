import React, { useState } from 'react';
import { Head, usePage } from '@inertiajs/inertia-react';
import MissionForm from '@/Components/MissionForm';
import Guest from '@/Layouts/Guest';
import MissionDisplay from '@/Components/MissionDisplay';
import AlertComponent from '@/Components/AlertComponent';
import MissionDisplaySimple from '@/Components/MissionDisplaySimple';


export default function Mission({ missions, factions, resources }) {

  const { errors, flash } = usePage().props
  
  const [showForm, setShowForm] = useState(false);
  const [showButton, setShowButton] = useState(true);

  const showFormFunc = () => {
    setShowForm(!showForm);
    setShowButton(!showButton);
  }

  console.log(missions)

  return (
    <Guest>
      <Head title="Missions" />
      <div id="MissionTab" className="flex flex-col items-center space-y-20">
        <h1 className="text-white text-3xl sm:text-9xl mt-20">MISSIONS</h1>
          {/*flash messages originate from HandleInertiaRequests and controller(MissionPageController)*/}
          <AlertComponent error={errors} success={flash.success}/> 

        <ul className="space-y-2 mx-20 w-3/4" id="MissionList">
          {missions.map(item => (
            <li key={item.id}>
              <div className="" id={"mission" + item.id}>
                <MissionDisplaySimple
                  action={"/missions"}
                  id={item.id}
                  name={item.name}
                  description={item.description}
                  image={item.image}
                  faction={item.faction_id}
                  resources={item.resources}
                  factions={factions}
                  state={item.state}
                />
              </div>
            </li>
          ))}
        </ul>
      </div>
    </Guest>
  )
}

import React from 'react';
import Guest from '@/Layouts/Guest';
import { Head } from '@inertiajs/inertia-react';
import BackgroundPlayer from '@/Components/BackgroundPlayer'
import Footer from '@/Components/Footer'
import Button from '@mui/material/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSteam } from '@fortawesome/free-brands-svg-icons'
import FactionIntros from '@/Components/FactionIntros'
import FactionStats from '@/Components/FactionStatsChart'
import $ from 'jquery';


export default function Home(props) {

  {console.log(props)}

  const dataPoints = [
    {
      name: 'Total points gathered',
      Precorics: 3000,
      "Vector Accelerate": 1398,
      "Operative United": 4500,
    },
  ];
  const dataPlayers = [{
    name: 'Total number of players',
    Precorics: props.factions.Precorics.players_count,
    "Vector Accelerate": props.factions["Vector Accelerate"].players_count,
    "Operative United": props.factions["Operative United"].players_count,
  }
  ];

  const dataMissions = [
    {
      name: 'Points for finished missions',
      Precorics:  props.factions.Precorics.mission_points,
      "Vector Accelerate":  props.factions["Vector Accelerate"].mission_points,
      "Operative United":  props.factions["Operative United"].mission_points,
    },
  ];
  return (
    <>
                        {console.log(props)}

      <Head title="Blastronaut" />
      <Guest>
        <div id="First part" className=" z-50 leading-6 w-full h-full relative">
          <BackgroundPlayer className="pointer-events-none" />
          <div className="flex justify-center sm:absolute sm:inset-x-0 sm:bottom-1/5 fatText text-white bg-main sm:bg-transparent">
            <Button target="_blank" href="https://store.steampowered.com/app/1392650/" style={{ background: '#891E2B', 'fontSize': '200%', 'padding': '8px 5%' }} sx={{ borderRadius: 15 }} variant="contained" size="large" endIcon={<FontAwesomeIcon icon={faSteam}></FontAwesomeIcon>} >WISHLIST NOW ON STEAM</Button>
          </div>
        </div>

        <div className="sm:mx-20 pt-32 bg-main">
          <h1 className="mx-auto mb-24 text-4xl sm:text-6xl text-center text-white fatText">
            EXPLORE. EXTRACT. PROFIT.
          </h1>
          <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
            BLASTRONAUT is a mining game in procedural alien world
          </p>
          <div className="sm:flex mx-3">
            <img className="flex-auto sm:w-1/2 mx-auto sm:mr-1 mb-2 sm:mb-0" src="https://cdn.akamai.steamstatic.com/steam/apps/1392650/ss_e7b299d33d3cfc75d0443695bf1a21ce63e450e2.jpg"></img>
            <img className="flex-auto sm:w-1/2 mx-auto sm:ml-1" src="https://cdn.akamai.steamstatic.com/steam/apps/1392650/ss_be1e1991211f383df291d38e77d21912e2da7d69.jpg"></img>
          </div>
          <div id="underVideo">
          <FactionIntros factions = {props.factions}></FactionIntros>
          </div>
          <div className="mt-14 h-auto sm:flex">
            <div className="w-full sm:w-1/2 mx-auto mb-8 sm:mb-auto h-56">
              <FactionStats data={dataPlayers}></FactionStats>
            </div>
            <div className="w-full sm:w-1/2 mx-auto mb-8 sm:mb-auto h-56">
              <FactionStats data={dataMissions}></FactionStats>
            </div>
          </div>
          <h1 className="landingPageText mt-10 mx-auto text-3xl text-center">
            If you are still deciding, try out the FREE demo!
          </h1>
          <iframe className="mx-auto my-10 w-2/3 h-64	 block text-center" src="https://store.steampowered.com/widget/1392650/"></iframe>
        </div>
        <Footer></Footer>
      </Guest>
    </>
  )
}

import React, { useState } from 'react';
import { Head, usePage } from '@inertiajs/inertia-react';
import MissionForm from '@/Components/MissionForm';
import MissionDisplay from '@/Components/MissionDisplay';
import AlertComponent from '@/Components/AlertComponent';
import Gamemaster from '@/Layouts/Gamemaster';



export default function Mission({ auth, missions, factions, resources }) {

  const { errors, flash } = usePage().props
  
  const [showForm, setShowForm] = useState(false);
  const [showButton, setShowButton] = useState(true);

  const showFormFunc = () => {
    setShowForm(!showForm);
    setShowButton(!showButton);
  }


  return (
    <Gamemaster auth={auth}>
      <Head title="Missions" />
      <div id="MissionTab" className="flex flex-col items-center space-y-20">
        <h1 className="text-white text-3xl sm:text-9xl mt-20">MISSIONS</h1>
          {/*flash messages originate from HandleInertiaRequests and controller(MissionPageController)*/}
          <AlertComponent error={errors} success={flash.success}/> 
        {showButton && (
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mb-4 rounded" onClick={showFormFunc}>Add new mission</button>
        )}
        {showForm && (
          <div className="flex items-center mx-20 w-3/4 pt-4 bg-gray-900 appearance-none border-2 border-gray-900 rounded" id="NewMissionDiv">
            <MissionForm
              factions={factions}
              action={"/gm/missions"}
              resources={resources}
              showFormFunc={showFormFunc}
            />
          </div>
        )}

        <ul className="space-y-2 mx-20 w-3/4" id="MissionList">
          {missions.map(item => (
            <li key={item.id}>
              <div className="" id={"mission" + item.id}>
                <MissionDisplay
                  action={"/gm/missions"}
                  id={item.id}
                  name={item.name}
                  description={item.description}
                  image={item.image}
                  state={item.state}
                  faction={item.faction_id}
                  factions={factions}
                  resources={item.resources}
                  allResources={resources}
                  repeatable={item.repeatable}
                />
              </div>
            </li>
          ))}
        </ul>
      </div>
    </Gamemaster>
  )
}

import React from 'react';
import Gamemaster from '@/Layouts/Gamemaster';
import Button from '@mui/material/Button';

export default function Presskit({auth}) {
    return (
        <Gamemaster auth={auth}>
            <div className="items-center text-justify mx-16">
                <img className="mx-auto mb-8 pt-10 w-full" src='https://cdn.akamai.steamstatic.com/steam/apps/1392650/header.jpg' />
                <div className='flex'>
                    <span className='w-2/6'>
                        <h1 className="mx-auto mb-10 text-4xl sm:text-6xl text-center text-white fatText">
                            Quick facts
                        </h1>
                        <p className="mx-auto  mb-10 text-2xl sm:text-3xl landingPageText">
                            name: Blastronaut <br />location: <br /> release date: <br /> platform: <br />price:
                        </p>
                        <Button target="_self" href="https://discord.gg/nzrffQYvhZ" style={{ position: 'relative', margin: '0 0 20% 0', bottom: "0px", width: '60%', height: '15%', background: '#4287f5' }}
                            sx={{ borderRadius: 15 }} variant="contained" size="large">
                            Request a copy
                        </Button>
                    </span>
                    <span className='w-4/6'>
                        <h1 className="mx-auto mb-10 text-4xl sm:text-6xl text-center text-white fatText">
                            Game Introduction
                        </h1>
                        <h2 className="mx-auto mb-4 text-3xl sm:text-4xl text-center text-white">
                            Mine resources! Upgrade your gear! Make Profit!
                        </h2>
                        <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                            It is a dangerous but lucrative job. Explore this vast procedurally generated world with your jetpack. Extract the minerals with an explosive gel. Expand your mining platform. But most importantly: stay alive.
                        </p>
                    </span>
                </div>
                <div className="flex">
                    <span className='w-1/2'>
                        <h1 className="mx-auto mb-10 text-4xl sm:text-6xl text-center text-white fatText">
                            Light logo
                        </h1>
                        <h1 className="mx-auto mb-10 text-4xl sm:text-6xl text-center text-white fatText">
                            Dark logo
                        </h1>
                    </span>
                    <span className='w-1/2'>
                        <h1 className="mx-auto text-4xl sm:text-6xl text-center text-white fatText">
                            Icon
                        </h1>
                    </span>
                </div>
                <span className='flex mb-10'>
                    <span className='w-1/2 mr-4'>
                        <img className='w-full' src='https://cdn.akamai.steamstatic.com/steam/apps/1392650/extras/steam_jungle.gif' />
                    </span>
                    <span className='w-1/2 ml-4'>
                        <img className='w-full' src='https://cdn.akamai.steamstatic.com/steam/apps/1392650/extras/steam_rocket.gif' />
                    </span>
                </span>
                <span className='flex mb-16'>
                    <img className="flex-auto sm:w-1/4 mx-auto sm:mr-1 mb-2 sm:mb-0" src="https://cdn.akamai.steamstatic.com/steam/apps/1392650/ss_e7b299d33d3cfc75d0443695bf1a21ce63e450e2.jpg"></img>
                    <img className="flex-auto sm:w-1/4 mx-auto sm:ml-1" src="https://cdn.akamai.steamstatic.com/steam/apps/1392650/ss_be1e1991211f383df291d38e77d21912e2da7d69.jpg"></img>
                    <img className="flex-auto sm:w-1/4 mx-auto sm:ml-1" src="https://cdn.akamai.steamstatic.com/steam/apps/1392650/ss_5455f55edab083a358da0f670f85252c002f778d.jpg"></img>
                    <img className="flex-auto sm:w-1/4 mx-auto sm:ml-1" src="https://cdn.akamai.steamstatic.com/steam/apps/1392650/ss_80310a422dad88fe8a462943520c3123b5453d6e.jpg"></img>
                </span>
                <div>
                    <h1 className="mx-auto text-4xl sm:text-6xl text-center text-white fatText">
                        Quotes/Articles/Press releases
                    </h1>
                    <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                        Press release 1
                    </p>
                    <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                        Press release 2
                    </p>
                    <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                        Press release 3
                    </p>
                </div>
                <div className='flex'>
                    <span className='w-7/12'>
                        <h1 className="mx-auto text-4xl sm:text-6xl text-center text-white fatText">
                            About me
                        </h1>
                        <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                            Whatever
                        </p>
                        <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                            People need
                        </p>
                        <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                            to know.
                        </p>
                    </span>
                    <span className='w-5/12'>
                        <h1 className="mx-auto text-4xl sm:text-6xl text-center text-white fatText">
                            Additional links
                        </h1>
                        <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                            Anything that would help other understand what this game is about and what I do.
                        </p>
                    </span>
                </div>
                <span>
                    <h1 className="mx-auto text-4xl sm:text-6xl text-center text-white fatText">
                        List of contributors to the project
                    </h1>
                    <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                        This project could not have been made without...
                    </p>
                </span>
                <p className="mx-auto  mb-16 text-2xl sm:text-3xl text-center landingPageText">
                    The media has premission to use all of this content for commercial use, they may use it at their own risk.
                </p>
            </div>
        </Gamemaster>
    );
}
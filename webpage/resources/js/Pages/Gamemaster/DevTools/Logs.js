import React from 'react';

import DevToolsFrame from '@/Layouts/DevToolsFrame';
import Gamemaster from '@/Layouts/Gamemaster';




export default function Logs({auth, contents }) {
    let newText = contents.split ('\n').map ((item, i) => <p className="text-left" key={i}>{item}</p>);
    
    return (
    <Gamemaster auth={auth}>  

        <DevToolsFrame page = "Logs" >
            {newText}

        </DevToolsFrame>
     </Gamemaster>
    )
}

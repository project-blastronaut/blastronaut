import React from 'react';

import { Head } from '@inertiajs/inertia-react';
import DevToolsFrame from '@/Layouts/DevToolsFrame';
import { ArrowNarrowLeftIcon } from '@heroicons/react/outline';
import Gamemaster from '@/Layouts/Gamemaster';





export default function MissionState({ auth, missions }) {
    
    
    const stateMap= new Map();
    stateMap.set("drafted",0);
    stateMap.set("voting",1);
    stateMap.set("active",2);
    stateMap.set("completed",3);
    stateMap.set("failed",4);

    //Group by state
    let result = missions.reduce(function (map, mission) {
        map[stateMap.get(mission.state)] = map[stateMap.get(mission.state)] || [];
        map[stateMap.get(mission.state)].push(mission);
        return map;
    }, Object.create(null));
    
    //Largest group
    let maxint=0;
    let states= Object.keys(result);
    states.forEach(function(key) {
        if(result[key].length >maxint) maxint=result[key].length;
    });

    //Add arrays for all states.
    const iterator1 = stateMap.values();
    
    let k = iterator1.next().value;
    while (k !== undefined){
        if (!(k in result)){
            result[k] =[];
        }
        k = iterator1.next().value;
    }

    //Fitting data to table format.
    states= Object.keys(result);

    let contents = [];
    for (let index = 0; index < maxint; index++) {
        let temp = [];
        states.forEach(function(key) {
            if (!(result[key][index])){
                temp.push("");
            }else{
                temp.push(result[key][index]);
            }
        });
        contents.push(temp);
    };

    return (
    <Gamemaster auth={auth}>  
        <DevToolsFrame page = "Mission States" >
            <div className="flex flex-col">
                <div className="w-full">
                    <div className="border-b border-gray-200 shadow">
                        <table className = "divide-y divide-x table-auto w-full">
                            <thead className="divide-y divide-x bg-gray-50">
                                <tr className="divide-x">
                                    <th className="px-6 py-2 text-xs text-gray-500">
                                        Drafted
                                    </th>
                                    <th className="px-6 py-2 text-xs text-gray-500">
                                        Currently Voting
                                    </th>
                                    <th className="px-6 py-2 text-xs text-gray-500">
                                        Ongoing
                                    </th>
                                    <th className="px-6 py-2 text-xs text-gray-500">
                                        Completed
                                    </th>
                                    <th className="px-6 py-2 text-xs text-gray-500">
                                        Failed
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="divide-y bg-white">
                                {contents.map(row =>
                                <tr className="divide-x">
                                    {row.map(item=>
                                    <td className="px-6 py-4">
                                        <div className="text-sm text-gray-900">
                                            {item !==undefined
                                            ?item.name
                                             :""
                                            }
                                        </div>
                                    </td>    
                                    )}
                                </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </DevToolsFrame>
     </Gamemaster>
    )
}

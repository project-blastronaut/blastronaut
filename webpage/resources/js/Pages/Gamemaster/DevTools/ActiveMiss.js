import React from 'react';
import ActiveMissionCard from '@/Components/ActiveMissionCard';
import DevToolsFrame from '@/Layouts/DevToolsFrame';
import Gamemaster from '@/Layouts/Gamemaster';



export default function ActiveMiss({ auth,  missions }) {
    const keys = Object.keys(missions);
    return (
        <Gamemaster auth = {auth}>  
            <DevToolsFrame page="Active Missions">
                {keys.map(key => <ActiveMissionCard mission = {missions[key]}/>) }
            </DevToolsFrame>
        </Gamemaster>
    )
}

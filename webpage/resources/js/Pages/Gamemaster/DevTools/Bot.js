import React, { useState } from 'react';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import BotVoteForm from '@/Components/BotVoteForm';
import BotCredoForm from '@/Components/BotCredoForm';
import Button from '@/Components/Button';
import DevToolsFrame from '@/Layouts/DevToolsFrame';
import { useForm} from '@inertiajs/inertia-react';
import ReactSelect from 'react-select';
import Gamemaster from '@/Layouts/Gamemaster';
import { NotificationContainer} from 'react-notifications';


export default function Bot({auth, votes} ) {
    

    return (
        <Gamemaster auth={auth}>   
            {console.log(votes)}
            <DevToolsFrame page="Bot Actions">
                <div className="w-full max-w-full mt-10">
                    <div className="flex flex-wrap justify-center w-full -mx-3 mb-6 grid-cols-2">
                        <BotVoteForm votes = {votes}/>
                        <BotCredoForm/>
                        <NotificationContainer/>
                    </div>
                </div>
                
            </DevToolsFrame>
        </Gamemaster>
    )
}

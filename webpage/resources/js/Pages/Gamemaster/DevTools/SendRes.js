import React, { useState } from 'react';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import Button from '@/Components/Button';
import DevToolsFrame from '@/Layouts/DevToolsFrame';
import { useForm } from '@inertiajs/inertia-react';
import ReactSelect from 'react-select';
import Gamemaster from '@/Layouts/Gamemaster';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';


export default function SendRes({ auth, missions }) {

  const [options, setOptions] = useState([]);

  const { data, transform, setData, post, processing, errors, reset } = useForm({
    mission: '',
    quantity: '',
    player_id: '',
    resource_id: ''
  });

  const onHandleChange = (event) => {
    setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
  };

  const submit = (e) => {
    e.preventDefault();
    post('sendres');
    NotificationManager.success('Success', 'Resources sent');
  };

  const missionOptions = [];
  missions.forEach((mission) => {
    missionOptions.push({ target: "mission", value: mission.id, label: mission.name })
  })

  let handleMissionChange = (event) => {
    setData(event.target, event.value);

    let options = [];

    missions.find(obj => obj.id == event.value).requirements.forEach(resource =>
      options.push({ value: resource.id, label: resource.name }));

    setOptions(options);
  }


  return (
    <Gamemaster auth={auth}>

      <DevToolsFrame page="Send Resources">
        <form id="sendResourcesForm" className="w-full max-w-full mt-10" onSubmit={submit}>
          <div className="flex flex-wrap justify-center w-full -mx-3 mb-6">
            <div className="w-2/3 px-3 mb-7">
              <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="mission"
                value="Mission">
              </Label>
              <ReactSelect className="required w-full block " id="mission" name="mission" selected={data.mission} onChange={handleMissionChange} options={missionOptions} />
            </div>
            <div className="w-2/3 px-3 mb-7">
              <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="player_id"
                value="Player ID">
              </Label>
              <Input
                type="number"
                name="player_id"
                className="mt-1 block w-full"
                handleChange={onHandleChange}
                required
              />
            </div>

            <div className="w-1/2 px-3">
              <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="resource"
                value="Resource">
              </Label>
              <ReactSelect className="required" id="resource" name="resource" options={options} onChange={(e) => data.resource_id = e.value}>
              </ReactSelect>
            </div>

            <div className="w-1/2 px-3">
              <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="quantity"
                value="Quantity">
              </Label>
              <Input
                type="number"
                name="quantity"
                className="mt-1 block w-full"
                handleChange={onHandleChange}
                required
              />
            </div>
          </div>
          <div className="md:flex justify-center flex md:items-center mb-20" >
            <Button className="w-1/10 shadow bg-gray-700 hover:bg-teal-320 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" processing={processing}>
              Send
            </Button>
          </div>
        </form>
        <NotificationContainer />
      </DevToolsFrame>
    </Gamemaster>
  )
}




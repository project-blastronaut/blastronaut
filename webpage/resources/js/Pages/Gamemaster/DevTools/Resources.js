import React,{useState} from 'react';

import DevToolsFrame from '@/Layouts/DevToolsFrame';
import Gamemaster from '@/Layouts/Gamemaster';
import { Inertia} from '@inertiajs/inertia';
import { usePage } from '@inertiajs/inertia-react';
import AlertComponent from '@/Components/AlertComponent';




export default function Resources({auth, resources }) {    

    const deleteResource = (resource_id) => {
        Inertia.delete("resources/"+resource_id);
    };

    const editResource = (resource_id) => {
        Inertia.get("resources/" +resource_id);
    };

    const createResource = () => {
        Inertia.get("resources/" +"0");
    };

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const { errors, flash } = usePage().props


    return (
    <Gamemaster auth={auth}>  
        {console.log(resources)}
        <DevToolsFrame page = "Resources" >
        <AlertComponent error={errors} success={flash.success}/> 

        <div className="flex flex-col">
                <div className="w-full p-5 mb-20">
                        <table className = "divide-y divide-x table-fixed w-full">
                            <thead className="divide-y divide-x bg-gray-50">
                                <tr className="divide-x">
                                    <th className="w-1/8 px-6 py-2 text-xs text-gray-500">
                                        ID
                                    </th>
                                    <th className="w-1/12 px-6 py-2 text-xs text-gray-500">
                                        Icon
                                    </th>
                                    <th className="w-1/2 px-6 py-2 text-xs text-gray-500">
                                        Name
                                    </th>
                                    <th className="w-1/8 px-6 py-2 text-xs text-gray-500">
                                        Value
                                    </th>
                                    <th className="w-1/8 px-6 py-2 text-xs text-gray-500">
                                        Actions
                                    </th>
                                   
                                </tr>
                            </thead>
                            <tbody className="divide-y bg-white">
                                {resources.map(resource =>(
                                    <tr key= {resource.id}className="divide-x">
                                        <td className=" py-4">
                                            <div className="text-xl text-gray-900">
                                                {resource.id}
                                            </div>
                                        </td>
                                        <td className="">
                                            <div className="text-xl text-gray-900 ">
                                                <img className ="object-contain object-contain h-full w-full" src={"/storage/"+resource.image} alt="Image"/>
                                            </div>
                                        </td>
                                        <td className="px-6 py-4">
                                            <div className="text-xl text-gray-900">
                                                {resource.name}
                                            </div>
                                        </td>
                                        <td className="px-6 py-4">
                                            <div className="text-xl text-gray-900">
                                                {resource.value}
                                            </div>
                                        </td>
                                        <td className="px-6 py-4">
                                            <div className="flex flex-row items-center space-x-4 mr-8">
                                                <button className="flex items-center bg-yellow-600 border border-transparent rounded-md text-3m  py-1 px-2 hover:bg-yellow-400" onClick={()=>editResource(resource.id)}>&#x270D;</button>
                                                <button className="flex items-center bg-red-600 border border-transparent rounded-md text-3m py-1 px-1 hover:bg-red-400" onClick={()=>deleteResource(resource.id)}>🗑️</button>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        <div className = " items-center mt-10 w-full">
                            <button className=" bg-green-600 border border-transparent rounded-md text-white text-3xl py-1 px-1 hover:bg-green-400" onClick={createResource}>New resource</button>
                        </div>
                </div>
            </div>
        </DevToolsFrame>
     </Gamemaster>
    )
}

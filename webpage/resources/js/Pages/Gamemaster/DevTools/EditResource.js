import React from 'react';

import DevToolsFrame from '@/Layouts/DevToolsFrame';
import Gamemaster from '@/Layouts/Gamemaster';
import { useForm } from '@inertiajs/inertia-react';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import { usePage } from '@inertiajs/inertia-react';
import AlertComponent from '@/Components/AlertComponent';




export default function EditResource({ auth, resource }) {
    const { errors, flash } = usePage().props

    const { data, setData, post, processing, reset } = useForm({
        name: resource.name,
        value: resource.value,
        image: '',
    });
    const onHandleChange = (event, isFile) => {
        
        if (isFile){
            setData(event.target.name, event.target.files[0]);
        } else {
            setData(event.target.name, event.target.value);
        }
    };
 

    const submit = (e) => {
        e.preventDefault();
        post('../resources/'+ resource.id);
    };

    return (
        <Gamemaster auth={auth}>
            {        console.log(resource)}
            <DevToolsFrame page="Resources" >
            <AlertComponent error={errors} success={flash.success}/> 

                <form className="w-full cmax-w-full  mt-8 mb-10" onSubmit={submit}>
                    <div className="flex flex-wrap flex-col justify-center items-center w-full mb-6">
                        <div className=" mb-10">
                            <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                forInput="name"
                                value="Name"
                            />
                            <Input
                                type="text"
                                name="name"
                                value={data.name}
                                className="mt-1 block w-full"
                                handleChange={(e) => { onHandleChange(e, false) }}
                            />
                        </div>
                        <div className=" mb-10">
                            <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                forInput="value"
                                value="Value"
                            />
                            <Input
                                type="text"
                                name="value"
                                value={data.value}
                                className="mt-1 block w-full"
                                handleChange={(e) => { onHandleChange(e, false) }}
                            />
                        </div>
                        <div className="flex flex-row space-x-14 divide-x">
                            <div className=" mb-3">
                                <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                            value={"Current"}
                                        />
                                <img className ="h-16 w-16" src={"/storage/"+resource.image} alt="Image"/>
                                </div>
                            <div className=" mb-3">
                                <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                    forInput="image"
                                    value={"Icon"}
                                />
                                <Input
                                    type="file"
                                    name="image"
                                    accept=".png"
                                    className="mt-1 block w-full"
                                    handleChange={(e) => { onHandleChange(e, true) }}
                                />
                            </div>
                        </div>
                   
                    </div>
                    <div className = " items-center mt-10 w-full">
                        <button className=" bg-blue-600 border border-transparent rounded-md text-white text-3m py-1 px-1 hover:bg-blue-400">Update</button>
                    </div>
                </form>
            </DevToolsFrame >
        </Gamemaster >
    )
}

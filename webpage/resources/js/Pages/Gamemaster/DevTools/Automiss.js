import React from 'react';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import Button from '@/Components/Button';
import DevToolsFrame from '@/Layouts/DevToolsFrame';
import { useForm } from '@inertiajs/inertia-react';
import Checkbox from '@/Components/Checkbox';
import Gamemaster from '@/Layouts/Gamemaster';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import axios from 'axios';

export default function AutoMiss({ auth, automatic, faction_data, s }) {
    const { data, transform, setData, post, processing, errors, reset } = useForm({
        faction_data: faction_data,
        automatic: automatic.automatic,
    });

    //transform((data) => ({
    //  f1_mission_time: data.mission_time,
    //f3_mission_time: data.mission_time,
    //f2_mission_time: data.mission_time,
    //f1_voting_time: data.voting_time,
    // f2_voting_time: data.voting_time,
    // f3_voting_time: data.voting_time,
    //automatic: data.automatic
    //}));

    const onHandleChange = (event, index) => {
        console.log("here");
        if (index !== undefined) {

            faction_data[index][event.target.name] = parseInt(event.target.value);
            setData('faction_data', faction_data);
            return;
        }
        setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
    };

    const submit = (e) => {
        e.preventDefault();
        axios.post('automiss', data).then(response => {
            NotificationManager.success('Changes saved');
        }
        ).catch(function (error) { //https://stackoverflow.com/questions/49967779/axios-handling-errors
            if (error.response) {
                // Request made and server responded
                NotificationManager.error(error.response.data, 'Error ' + error.response.status);
            };
        });
    };

    return (
        <Gamemaster auth={auth}>
            <DevToolsFrame page="Automatic Missions">
                <form className="w-full cmax-w-full  mt-10" onSubmit={submit}>
                    <div className="flex flex-wrap flex-col justify-center items-center w-full -mx-3 mb-6">
                        <div className="mb-10">
                            <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                forInput="automatic"
                                value="Automatic Dispatching"
                            />
                            <Checkbox
                                type="checkbox"
                                name="automatic"
                                value={data.automatic}
                                checked={data.automatic}
                                className="mt-1 block w-full"
                                isFocused={true}
                                handleChange={onHandleChange}
                            />
                        </div>
                        <div className="flex flex-wrap divide-x  divide-black flex-row">
                            {faction_data.map((faction, index) =>
                                <div className="flex flex-wrap flex-col  justify-center items-center w-1/3">
                                    <Label className="block uppercase tracking-wide text-gray-700 text-xl font-bold mb-2"
                                        value={faction.name}
                                    />
                                    <div className="w-3/4 mb-10 ">
                                        <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                            forInput="playerbase_size"
                                            value={"Playerbase size"}
                                        />
                                        <Input
                                            type="number"
                                            name="playerbase_size"
                                            value={faction.playerbase_size}
                                            className="mt-1 block w-full"
                                            handleChange={(e) => { onHandleChange(e, index) }}
                                        />
                                    </div>
                                    <div className="w-3/4 mb-10 ">
                                        <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                            forInput="voting_time_mins"
                                            value={"Voting length minutes"}
                                        />
                                        <Input
                                            type="number"
                                            name="voting_time_mins"
                                            value={faction.voting_time_mins}
                                            className="mt-1 block w-full"
                                            handleChange={(e) => { onHandleChange(e, index) }}
                                        />
                                    </div>
                                    <div className="w-3/4 mb-3 ">
                                        <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2"
                                            forInput="mission_time_mins"
                                            value="Mission time minutes"
                                        />
                                        <Input
                                            type="number"
                                            name="mission_time_mins"
                                            value={faction.mission_time_mins}
                                            className="mt-1 block w-full"
                                            handleChange={(e) => { onHandleChange(e, index) }}
                                        />
                                    </div>

                                </div>
                            )}
                        </div>
                        <div className="flex items-center justify-end mt-4">
                            <Button className="w-1/10" processing={processing}>
                                Confirm Changes
                            </Button>
                            <NotificationContainer />
                        </div>
                    </div>
                </form>
            </DevToolsFrame>
        </Gamemaster>
    )
}

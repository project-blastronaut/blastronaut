import React from 'react';
import NavBar from '@/Components/NavBar';
import { Head } from '@inertiajs/inertia-react';
import DevToolsFrame from '@/Layouts/DevToolsFrame';
import Gamemaster from '@/Layouts/Gamemaster';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';



export default function DispatchMiss({ auth, missions, resources }) {

  function sendMessage(event) {
    event.preventDefault();

    var webhook = "https://discord.com/api/webhooks/895934806888685608/5d8H7m_4RmyjsOnUJhMKa-3AQihz9iAtsMtlND_jhNQ9sIbVKXgmhuJWHhbYCTfb2HSo"

    var faction = parseInt(event.target.faction.value)
    var duration = event.target.duration_days.value * 24 * 60 + event.target.duration_hours.value * 60 + event.target.duration_minutes.value * 1;
    var mission_id = event.target.mission_id.value
    var mission_text = event.target.mission_text.value
    var mission_thumbnail_url = event.target.mission_thumbnail_url.value

    let payload = {
      "command_name": "create-social-mission",
      "mission_faction": faction,
      "mission_id": mission_id,
      "mission_duration": duration,
      "mission_text": mission_text,
      "mission_thumbnail_url": mission_thumbnail_url
    };
    console.log(JSON.stringify(payload));

    const request = new XMLHttpRequest();
    request.open("POST", webhook);
    request.setRequestHeader('Content-type', 'application/json');
    const request_body = {
      content: "```json\r" + JSON.stringify(payload) + "```"
    }
    request.send(JSON.stringify(request_body));
  }

  return (
    <Gamemaster auth={auth}>

      <Head title="Blastronaut" />
      <DevToolsFrame page="Social Mission Dispatch" >
        <form id="socialMissionForm" className="w-full max-w-full mt-10" onSubmit={sendMessage}>
          <div className="flex justify-center flex-wrap -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-800 text-s font-bold " htmlFor="grid-password">
                Faction
              </label>
              <label className="inline-flex items-center">
                <input type="radio" className="form-radio" name="faction" value="1" required />
                <span className="ml-2">Operative United</span>
              </label>
              <label className="inline-flex items-center ml-6">
                <input type="radio" className="form-radio" name="faction" value="2" required />
                <span className="ml-2">Vector Accelerate</span>
              </label>
              <label className="inline-flex items-center ml-6">
                <input type="radio" className="form-radio" name="faction" value="3" required />
                <span className="ml-2">Precorics</span>
              </label>
              <label className="inline-flex items-center ml-6">
                <input type="radio" className="form-radio" name="faction" value="4" required />
                <span className="ml-2">Interstellar</span>
              </label>
            </div>
          </div>

          <div className="flex flex-wrap justify-center -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" htmlFor="grid-password">
                Social mission duration
              </label>
            </div>
            <div className="inline-flex">
              <div className="md:w-1/3 px-3">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="duration_days">
                  Days
                </label>
                <input type="number" name="duration_days" id="duration_days" required></input>
              </div>
              <div className="md:w-1/3 px-3">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="duration_hours">
                  Hours
                </label>
                <input type="number" name="duration_hours" id="duration_hours" required></input>
              </div>
              <div className="md:w-1/3 px-3">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="duration_minutes">
                  Minutes
                </label>
                <input type="number" name="duration_minutes" id="duration_minutes" required></input>
              </div>
            </div>
          </div>
          <div className="flex flex-wrap justify-center -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" htmlFor="grid-password">
                Mission ID
              </label>
            </div>
            <div className="inline-flex">
              <input type="number" name="mission_id" id="mission_id" required></input>
            </div>
          </div>
          <div className="flex flex-wrap justify-center -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" htmlFor="grid-password">
                Mission text
              </label>
            </div>
            <div className="inline-flex">
              <input type="text" name="mission_text" id="mission_text" required></input>
            </div>
          </div>
          <div className="flex flex-wrap justify-center -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" htmlFor="grid-password">
                Mission thumbnail (URL)
              </label>
            </div>
            <div className="inline-flex">
              <input type="text" name="mission_thumbnail_url" id="mission_thumbnail_url" required></input>
            </div>
          </div>

          <div className="flex  justify-center md:items-center">
            <button className="shadow bg-gray-700 hover:bg-teal-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit" onClick={() => {
              //https://stackoverflow.com/questions/57087145/check-if-all-required-fields-are-filled-in-a-specific-div
              let allAreFilled = true;
              document.getElementById("socialMissionForm").querySelectorAll("[required]").forEach(function (i) {
                if (!allAreFilled) return;
                if (!i.value) allAreFilled = false;
                if (i.type === "radio") {
                  let radioValueCheck = false;
                  document.getElementById("socialMissionForm").querySelectorAll(`[name=${i.name}]`).forEach(function (r) {
                    if (r.checked) radioValueCheck = true;
                  })
                  allAreFilled = radioValueCheck;
                }
              })
              if (!allAreFilled) {
                NotificationManager.error('Fill all fields', 'Some fields are not filled!');
              }
              else {
                NotificationManager.success('Great success!', 'Mission dispatched');
              }
            }
            }>
              Send
            </button>
            <NotificationContainer />
          </div>
        </form>
      </DevToolsFrame>
    </Gamemaster>
  )
}

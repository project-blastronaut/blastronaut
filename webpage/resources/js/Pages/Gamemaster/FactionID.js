import React from 'react';
import { Head, usePage } from '@inertiajs/inertia-react';
import PlayerDisplay from '@/Components/PlayerDisplay';
import MissionDisplaySimple from '@/Components/MissionDisplaySimple';
import Gamemaster from '@/Layouts/Gamemaster';


const mockPlayers = [
    { id: 1, name: 'Ülo#6969', Credo: '69420', image: 'https://cdn.discordapp.com/avatars/217624637406838785/3cbbf7fb951748d081c04409d0b36b42.webp?size=160' },
    { id: 2, name: 'Saamuel#6667', Credo: '39456', image: 'https://cdn.discordapp.com/avatars/217624637406838785/3cbbf7fb951748d081c04409d0b36b42.webp?size=160' },
    { id: 3, name: 'Rafaello#1234', Credo: '756', image: 'https://cdn.discordapp.com/avatars/217624637406838785/3cbbf7fb951748d081c04409d0b36b42.webp?size=160' },
];

export default function FactionID(props) {

    return (
        <Gamemaster auth = {props.auth}>
            <Head title={props.faction.name} />
            <div className="flex flex-col items-center space-y-20">
                <h1 className="text-white text-9xl mt-20">{props.faction.name.toUpperCase()}</h1>
                <div className="flex flex-row items-start mx-20 space-x-10">
                    <div className="w-1/2">
                        <p className="text-5xl text-white">PAST PROJECTS</p>
                        <ul className="space-y-2 w-full">
                            {props.missions.map(item => (
                                <li key={item.id}>
                                    <div className="">
                                        <MissionDisplaySimple
                                            action={"/factions"}
                                            id={item.id}
                                            name={item.name}
                                            description={item.description}
                                            image={item.image}
                                            faction={props.faction.id}
                                            resources={item.resources}

                                        />
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>

                    <div className="w-1/2">
                        <p className="text-5xl text-white">PLAYER LEADERBOARD</p>
                        <ul className="space-y-2 w-full">
                            {props.players.map(item => (
                                <li key={item.id}>
                                    <div className="">
                                        <PlayerDisplay
                                            action={"/factions"}
                                            name={item.name}
                                            credo={item.credo}
                                            image={item.image}
                                            faction={props.faction.id}
                                        />
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>

        </Gamemaster>
    );
}
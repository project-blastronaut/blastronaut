import React, { useState } from 'react';
import Dropdown from '@/Components/Dropdown';
import ResponsiveNavLink from '@/Components/ResponsiveNavLink';
import NavBar from '@/Components/NavBar'; 

export default function Gamemaster({ page, auth, header, children }) {
    const [showingNavigationDropdown, setShowingNavigationDropdown] = useState(false);

    return (
        <div>
            <NavBar auth= {auth} active={page}></NavBar>

            <main>{children}</main>
        </div>
    );
}

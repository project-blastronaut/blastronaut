import React from 'react';
import { Link } from '@inertiajs/inertia-react';
import NavBarGuest from '@/Components/NavBarGuest';

export default function Guest({ children }) {
    return (
    <div>
        <NavBarGuest></NavBarGuest>
        <main>{children}</main>
    </div>
    )
}

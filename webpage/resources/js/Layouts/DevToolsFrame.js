import React from 'react';
import ApplicationLogo from '@/Components/ApplicationLogo';
import { Link, Head } from '@inertiajs/inertia-react';
import NavBar from '@/Components/NavBar';
import DevNavigation from '@/Components/DevNavigation';

export default function DevToolsFrame({ children, page }) {
    return (
        <> 
            <Head title="Blastronaut"/>
                <div className="grid grid-cols-1 md:grid-cols-12">
                    <div className="md:col-start-4 md:col-span-6" >
                        <div className="flex mt-10 flex-col bg-gray-200 ">
                            <DevNavigation active= {page}/>
                            <h2 className="text-red-600 font-bold " >Development only</h2>
                            <div>
                                {children}
                            </div>
                        </div>
                    </div>
            </div>
        </>    
    );
}

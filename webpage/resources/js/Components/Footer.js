import { faTwitterSquare, faFacebookSquare, faYoutube, faItchIo, faDiscord, faSteamSquare } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import IconButton from '@mui/material/IconButton';

const icons = [
    { name: faTwitterSquare, href: 'https://twitter.com/JaanusJaggo' },
    { name: faFacebookSquare, href: 'https://www.facebook.com/perfoon' },
    { name: faYoutube, href: 'https://www.youtube.com/c/Perfoon' },
    { name: faItchIo, href: 'https://perfoon.itch.io/blastronaut' },
    { name: faDiscord, href: 'https://discord.gg/nzrffQYvhZ' },
    { name: faSteamSquare, href: 'https://store.steampowered.com/app/1392650/' },
]

export default function Footer() {
    return (
        <div className='mb-8'>
            {icons.map((item) => (
                <IconButton key={item.href} href={item.href} className='my-32'>
                    <FontAwesomeIcon icon={item.name} color='white' size='3x'></FontAwesomeIcon>
                </IconButton>
            ))}
        </div>
    )
}
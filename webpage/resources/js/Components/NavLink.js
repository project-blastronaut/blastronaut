import React from 'react';
import { Link } from '@inertiajs/inertia-react';

export default function NavLink({ href, active, children, color }) {
    return (
        <Link
            href={href}
            className={
                active
                    ? 'inline-flexbg-'+{color}+'  items-center px-1 pt-1 border-b-2 border-indigo-400 text-2xl font-medium leading-5 text-white focus:outline-none focus:border-indigo-700 transition duration-150 ease-in-out'
                    : 'inline-flex bg-'+{color}+' items-center px-1 pt-1 border-b-2 border-transparent text-xl font-medium leading-5 text-white hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out'
            }
        >
            {children}
        </Link>
    );
}


import React from 'react';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import Button from '@/Components/Button';
import { useForm} from '@inertiajs/inertia-react';
import { NotificationManager } from 'react-notifications';

export default function BotCredoForm() {

    const { data, transform, setData, post, processing, errors, reset } = useForm({
        vote_id: '',
        winner_id: '',
    });

    const submitCredo = (e) => {
        e.preventDefault();
        post('/api/discordBot/credo', );
        NotificationManager.success('Success', 'Credit points sent');
    };

    const onHandleChange = (event) => {
        setData(event.target.name, event.target.value);
    };



return (
    <form className="w-1/3 bg-gray-100 p-10 " onSubmit={submitCredo}>
        <Label className="block uppercase tracking-wide text-gray-700 text-lg font-bold mb-10"
            value="Credo transaction">
        </Label>
        <div className="w-full px-3 mb-7">
            <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="player_id"
                value="Player ID">
            </Label>
            <Input
                type="number"
                name="player_id"
                className="mt-1 block w-full"
                handleChange={onHandleChange}
                required
            />
        </div>
        <div className="w-full px-3 mb-7">
            <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="credo"
                value="Credo">
            </Label>
            <Input
                type="number"
                name="credo"
                className="mt-1 block w-full"
                handleChange={onHandleChange}
                required
            />
        </div>

        <div className="w-1/3 md:flex justify-center flex md:items-center mt-5">
            <Button className="shadow bg-gray-700 hover:bg-teal-320 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" processing={processing}>
                Send
            </Button>
        </div>
    </form>
    )
}
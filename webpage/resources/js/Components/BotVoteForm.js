import React, { useState } from 'react';
import Label from '@/Components/Label';
import Button from '@/Components/Button';
import { useForm} from '@inertiajs/inertia-react';
import ReactSelect from 'react-select';
import { NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

export default function BotVoteForm( {votes} ) {

    const [options, setOptions] = useState([]);
    const { data, transform, setData, post, processing, errors, reset } = useForm({
        vote_id: '',
        winner_id: '',

    });
    const submitMission = (e) => {
        e.preventDefault();
        post('bot');
        NotificationManager.success('Success', 'Vote winner submited');
    };

    const voteOptions = [];
    votes.forEach((vote) => {
        voteOptions.push({ target: "vote_id", value: vote.id, label: vote.faction_id })
    })

    let handleVoteChange = (event) => {
        setData(event.target, event.value);
        console.log(data);
        let options = [];

        votes.find(vote => vote.id == event.value).missions.forEach(mission =>
            options.push({target: "winner_id", value: mission.id, label: mission.name }));
    
        setOptions(options);
    }



    return (
        <form className="w-1/3 bg-gray-100 p-10 mr-5 " onSubmit={submitMission}>
            <Label className="block uppercase tracking-wide text-gray-700 text-lg font-bold mb-10"
                value="Vote winner">
            </Label>
            <div className="w-full px-3 mb-7">
                <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="vote_id"
                    value="Vote">
                </Label>
                <ReactSelect className="required w-full block " name="vote_id" onChange={handleVoteChange} options={voteOptions} />
            </div>
            <div className="w-full px-3">
                <Label className="block uppercase tracking-wide text-gray-700 text-s font-bold mb-2" forInput="winner_id"
                    value="mission">
                </Label>
                <ReactSelect className="required" id="winner_id" name="winner_id" options={options} onChange={(e) => data.winner_id = e.value}>
                </ReactSelect>
            </div>
            <div className="w-1/3 md:flex justify-center flex md:items-center mt-10">
                <Button className="shadow bg-gray-700 hover:bg-teal-320 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" processing={processing}>
                    Send
                </Button>
            </div>
        </form>
    )
}
import React, { PureComponent } from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


export default class FactionStatsChart extends PureComponent {
    //   static demoUrl = 'https://codesandbox.io/s/simple-bar-chart-tpz8r';

    render() {
    return (
      <ResponsiveContainer width="100%" height="100%">
        <BarChart
          width={500}
          height={300}
          data={this.props.data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="Precorics" fill="#42A46D" />
          <Bar dataKey="Vector Accelerate" fill="#7F52C8" />
          <Bar dataKey="Operative United" fill="#FFAD33" />
        </BarChart>
        
      </ResponsiveContainer>
    );
  }
}

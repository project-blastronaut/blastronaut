import React, { useState } from 'react';

export default function ResourceDisplay(props) {

    const collected = props.resource.collected
    const specified_amount = props.resource.specified_amount
    let amountString = props.resource.amount
    
    if (collected !== null && specified_amount !== null) amountString = collected + "/" + specified_amount

    return (
        <>
            <div className="flex w-full self-center items-center space-x-4">
                <img className="w-8 h-8" src={"/storage/" + props.resource.image}></img>
                <p className="text-xl">{props.resource.name}</p>
                <p className="text-xl">{amountString}</p>
            </div>
        </>
    );
}

import React from 'react';

export default function DevNavigation({active}) {
   let items = [
    { name: 'Mission Dispatch', href: '/gm/devtools/dispatch' },
    { name: 'Send Resources', href: '/gm/devtools/sendres'},
    { name: 'Automatic Missions', href: '/gm/devtools/automiss'},
    { name: 'Mission States', href: '/gm/devtools/missionstatus'},
    { name: 'Active Missions', href: '/gm/devtools/activemiss'},
    { name: 'Logs', href: '/gm/devtools/logs'},
    { name: 'Bot Actions', href: '/gm/devtools/bot'},
    { name: 'Resources', href: '/gm/devtools/resources'},
   ];
    return (
        <ul className="flex border-b mb-10 justify-center mt-20">
          {items.map((item) => 
            <li key={item.name} className="-mb-px mr-3">
              {item.name === active
                ? <a className="w-full bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-gray-800 font-semibold" href={item.href}>{item.name}</a>
                : <a className="w-full bg-white inline-block py-2 px-4 text-gray-400 hover:text-gray-800 font-semibold" href={item.href}>{item.name}</a>
              }
            </li>
          )}
        </ul>
    );
    
}




import { Inertia } from '@inertiajs/inertia';
import { usePage } from '@inertiajs/inertia-react'
import React, { useState, Fragment } from 'react'

export default function MissionForm(props) {

    const [repeatable, setRepeatable] = useState(true);
    const toggleClass = ' transform translate-x-5 bg-blue-600';

    const reverseRepeatable = () => {
        setRepeatable(!repeatable);
    }

    const resources = props.resources;//['cobblestone', 'slime', 'niter', 'palladium']
    const AddedElement = () => <div className="flex flex-row items-center space-x-4">
        <select className="flex bg-gray-200 appearance-none border-2 border-gray-200 rounded w-48 p-5 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
            type="text"
            name="resource_id"
            placeholder="Resource">

            {resources.map((item) => (
                <option value={item.id}>{item.name}</option>
            ))}

        </select>
        <input className="flex bg-gray-200 appearance-none border-2 border-gray-200 rounded w-48 p-5 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            name="resource_quantity"
            defaultValue="0"
        />
    </div >


    var handleSubmit = (event) => {
        event.preventDefault();
        const quants = event.target.resource_quantity;
        const ids = event.target.resource_id;

        let pairs = [];
        console.log(event);
        if (NodeList.prototype.isPrototypeOf(quants)) {
            for (let i = 0; i < quants.length; i++) {
                pairs.push([ids[i].value, quants[i].value]);
            }
        } else {
            pairs.push([ids.value, quants.value])
        }

        const packets = {
            name: event.target.name.value,
            faction_id: event.target.faction_id.value,
            mission_desc: event.target.description.value,
            mission_thumbnail: event.target.thumbnail.value,
            miss_resources: pairs,
            repeatable: repeatable
        };
        Inertia.post(props.action, packets);

    }
    const [reqList, setReqList] = useState([AddedElement]);



    return (
        <>
            <form className="flex flex-col items-center w-full space-y-4 p-4 text-xl" onSubmit={handleSubmit}>
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="name"
                    id="mission-name"
                    placeholder="Mission name"
                />
                <select className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="faction_id"
                    id="mission-faction"
                    placeholder="Mission faction">

                    {props.factions.map((item) => (
                        <option key={item.id} value={item.id}>{item.name}</option>
                    ))}

                </select>
                <textarea className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="description"
                    id="mission-description"
                    placeholder="Mission description"
                />
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="thumbnail"
                    id="mission-thumbnail"
                    placeholder="Mission thumbnail URL"
                />
                <Fragment>
                    {reqList.map((req, i) => React.createElement(req, { key: i }))}
                </Fragment>
                <div className="items-center space-x-4 py-2">
                    <button onClick={() => {
                        if (reqList.length < 3) {
                            let newList = [...reqList]
                            newList.push(AddedElement);
                            setReqList(newList)
                        }
                    }}
                        type="button"
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded">+</button>
                    <button onClick={() => {
                        if (reqList.length > 1) {
                            let newList = [...reqList]
                            newList.pop();
                            setReqList(newList)
                        }
                    }}
                        type="button"
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded">-</button>
                </div>
                <div className="md:w-14 md:h-7 w-12 h-6 flex items-center bg-gray-300 rounded-full p-1 cursor-pointer"
                    onClick={reverseRepeatable}
                >
                    <div className={"bg-white md:w-6 md:h-6 h-5 w-5 rounded-full shadow-md transform" + (!repeatable ? null : toggleClass)}>
                    </div>
                </div>
                <div className="items-center space-x-4 py-2">
                    <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded" type="submit">Submit</button>
                    <button className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded" onClick={props.showFormFunc}> Cancel</button>
                </div>
            </form>

        </>
    )
}
import Button from '@mui/material/Button';
const navigation = [
  { name: 'Home', href: '/' , color:'#212E38', radius:2},
  { name: 'Missions', href: '/missions' , color:'#212E38', radius:2},
  { name: 'Factions', href: '/factions' , color:'#212E38', radius:2},
  { name: 'Presskit', href: '/presskit' , color:'#212E38', radius:2},
  { name: 'Login', href: '/login' , color:'#212E38', radius:2},
  { name: 'Steam', href: 'https://store.steampowered.com/app/1392650/', color:'#891E2B', radius:15, class:'fatText'},
]


function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function NavBarGuest({ active }) {
  return (
          <div className="z-40 fixed flex bg-main w-full bg-backestBack">
            {navigation.map((item) => (
              <Button key={item.name} className={item.class} href={item.href} style={{margin:'1% auto', padding:'0 3%' , background: item.color, color: 'white'}}
              sx={{ borderRadius: item.radius }}>
              {item.name}
          </Button>
            ))}
          </div>
  )
}
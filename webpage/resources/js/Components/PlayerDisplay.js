import React from 'react';

export default function PlayerDisplay(props) {

    let credo = props.credo;
    let name = props.name;
    if (credo == null) credo = 0
    if (name == null) name = "Unknown"

    return (
        <>
            <div className="flex flex-row items-center space-x-4 text-white bg-gray-700 rounded-md">
                <div className="flex flex-row flex-grow">
                    <img className="h-24 w-24 rounded-full m-5" src={props.image}></img>
                </div>
                <div className="flex flex-col w-3/4">
                    <p className="text-3xl">{name}</p>
                </div>
                <div className="flex flex-row items-center space-x-4 px-5 mr-8">
                    <p className="text-3xl text-yellow-400">{credo}</p>
                </div>
            </div>
        </>
    );
}
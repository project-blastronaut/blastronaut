import React from 'react';
import { Inertia } from '@inertiajs/inertia';
const { useState, Fragment } = React

export default function MissionEditForm(props) {

    const allResources = props.allResources;
    const resDict = {};
    const toBeAddedToReqList = [];
    allResources.map((item) => (
        resDict[item.id] = item.name
    ))

    const [repeatable, setRepeatable] = useState(props.repeatable);
    const toggleClass = ' transform translate-x-5 bg-blue-600';

    const reverseRepeatable = () => {
        //console.log("clickity" + repeatable)
        setRepeatable(!repeatable);
    }


    props.resources.map((res) => (

        toBeAddedToReqList.push(
            () => <div className="flex flex-row items-center space-x-4">
                <select className="flex bg-gray-200 appearance-none border-2 border-gray-200 rounded w-48 p-5 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="resource_id"
                    defaultValue={res.id}
                    placeholder="Resource">

                    {allResources.map((item) => (
                        <option value={item.id}>{item.name}</option>
                    ))}

                </select>
                <input className="flex bg-gray-200 appearance-none border-2 border-gray-200 rounded w-48 p-5 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="number"
                    name="resource_quantity"
                    defaultValue={res.amount}
                    placeholder="100"
                />
            </div>
        )
    ))
    const [reqList, setReqList] = useState(toBeAddedToReqList);

    const AddedElement = () => <div className="flex flex-row items-center space-x-4">
        <select className="flex bg-gray-200 appearance-none border-2 border-gray-200 rounded w-48 p-5 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
            type="text"
            name="resource_id"
            placeholder="Resource">

            {allResources.map((item) => (
                <option value={item.id}>{item.name}</option>
            ))}

        </select>
        <input className="flex bg-gray-200 appearance-none border-2 border-gray-200 rounded w-48 p-5 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
            type="number"
            name="resource_quantity"
            defaultValue="0"
            
        />
    </div>

    var handleSubmit = (event) => {
        event.preventDefault();
        const quants = event.target.resource_quantity;
        const ids = event.target.resource_id;


        let pairs = [];

        if (NodeList.prototype.isPrototypeOf(quants)) {
            for (let i = 0; i < quants.length; i++) {
                pairs.push([ids[i].value, quants[i].value]);
            }
        } else {
            pairs.push([ids.value, quants.value])
        }

        const packets = {
            name: event.target.name.value,
            faction_id: event.target.faction_id.value,
            mission_desc: event.target.description.value,
            mission_thumbnail: event.target.thumbnail.value,
            miss_resources: pairs,
            repeatable: repeatable
        };
        Inertia.put(props.action, packets);
        showFormFunc();
    }

    const showFormFunc = () => {
        props.showFormFunc();
    }

    return (
        <>
            <form className="flex flex-col items-center w-full space-y-4 p-4 text-xl bg-gray-900" onSubmit={handleSubmit}>
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="name"
                    defaultValue={props.name}
                    id="mission-name"
                    placeholder="Mission name"
                />
                <select className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="faction_id"
                    defaultValue={props.faction}
                    id="mission-faction"
                    placeholder="Mission faction">

                    {props.factions.map((item) => (
                        <option value={item.id}>{item.name}</option>
                    ))}

                </select>
                <textarea className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="description"
                    defaultValue={props.description}
                    id="mission-description"
                    placeholder="Mission description"
                />
                <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    name="thumbnail"
                    defaultValue={props.image}
                    id="mission-thumbnail"
                    placeholder="Mission thumbnail URL"
                />
                <Fragment>
                    {reqList.map((req, i) => React.createElement(req, { key: i }))}
                </Fragment>
                <div className="items-center space-x-4 py-2">
                    <button onClick={() => {
                        if (reqList.length < 3) {
                            let newList = [...reqList]
                            newList.push(AddedElement);
                            setReqList(newList)
                        }
                    }}
                        type="button"
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded">+</button>
                    <button onClick={() => {
                        if (reqList.length > 1) {
                            let newList = [...reqList]
                            newList.pop();
                            setReqList(newList)
                        }
                    }}
                        type="button"
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded">-</button>
                </div>
                <div className="md:w-14 md:h-7 w-12 h-6 flex items-center bg-gray-300 rounded-full p-1 cursor-pointer"
                    onClick={reverseRepeatable}
                >
                    <div className={"bg-white md:w-6 md:h-6 h-5 w-5 rounded-full shadow-md transform" + (!repeatable ? null : toggleClass)}>
                    </div>
                </div>
                <div className="items-center space-x-4 py-2">
                    <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded" type="submit">Update</button>
                    <button className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 mb-4 w-48 rounded" onClick={showFormFunc}>Cancel</button>
                </div>
            </form>

        </>
    )
}
import precoricsIcon from '../../pictures/precorics.png'
import operativeIcon from '../../pictures/operativeunited.png'
import vectorIcon from '../../pictures/vectoraccelerate.png'
import TopPlayers from './FactionTopPlayers'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from '@mui/material';
import { faDiscord } from '@fortawesome/free-brands-svg-icons';



export default function FactionIntros(props) {

    const factions = [
        { name: 'Precorics', color: 'bg-precorics', icon: precoricsIcon, buttonColor: '#21645C' },
        { name: 'Vector Accelerate', color: 'bg-vector', icon: vectorIcon, buttonColor: '#093475' },
        { name: 'Operative United', color: 'bg-operative', icon: operativeIcon, buttonColor: '#642835' },
    ];
    
    const colors= ["yellow-400", "gray-400", "yellow-700"];
    const titles= ["I", "II", "III"];
    const order= [2, 1, 3];
    const bar_sizes = [14,10,6];

    return (
        <div className='sm:flex my-14 sm:my-28'>
                    {console.log(props)}

            {factions.map((faction) => (
                <div key={faction.id} className={"sm:flex-1 py-4 sm:pb-12 mx-4 px-auto " + faction.color+" mb-5 sm:mb-0"}>
                    <img className="mx-auto" src={faction.icon} />
                    <h2 className='text-white fatText text-4xl sm:text-5xl'>{faction.name}</h2>
                    <TopPlayers topPlayers ={props.factions[faction.name].players} colors={colors} bar_sizes={bar_sizes} titles= {titles} order={order}></TopPlayers>
                    <Button target="_self" href="https://discord.gg/nzrffQYvhZ" style={{ position: 'relative', margin: '0 0 20% 0', bottom: "0px", width: '60%', height: '15%', background: faction.buttonColor }}
                        sx={{ borderRadius: 15 }} variant="contained" size="large"
                        endIcon={
                            <FontAwesomeIcon icon={faDiscord} color='white'>
                            </FontAwesomeIcon>}>
                        JOIN OUR DISCORD
                    </Button>
                </div>
            ))}
        </div>

    )
}
import React, { useState } from 'react';
import ResourceDisplay from '@/Components/ResourceDisplay';

export default function MissionDisplaySimple(props) {

    let colour;

    switch (props.faction) {
        case 3:
            colour = " bg-green-800 hover:bg-precorics"
            break;
        case 2:
            colour = " bg-purple-800 hover:bg-vector"
            break;
        case 1:
            colour = " bg-yellow-800 hover:bg-operative"
            break;
        default:
            break;
    }

    const [showResources, setShowResources] = useState(false);

    const showResourcesFunc = () => {
        setShowResources(!showResources);
    }

    return (
        <>
            {/*console.log(props)*/}
            <div className={"flex flex-col items-start text-white" + colour}>
                <div className={"flex flex-row w-full items-center space-x-4 text-white" + colour}>
                    <div className="flex flex-row flex-grow">
                        <img className="h-44 w-44 min-h-44 min-w-44" src={props.image}></img>
                    </div>
                    <div className="flex flex-col w-3/4">
                        <p className="text-3xl">{props.name}</p>
                        <p className="text-xl">{props.description}</p>
                        <p className="text-xl">{props.state}</p>
                        <div className="flex flex-row space-x-4">
                            {props.resources.map((item) => (
                                <div className="" id={item.id}>
                                    <ResourceDisplay
                                        resource={item}
                                    />
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
        
            </div>
        </>
    );
}
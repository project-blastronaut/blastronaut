import React from "react"
import ReactPlayer from "react-player"


const BackgroundPlayer = () => {
    return (
        <div className='player-wrapper'>
            <ReactPlayer
                className='react-player'
                url='https://www.optimatica.eu/cpy/blastronaut/BlastronautGameWavePitchVideo.mp4'
                width='100%'
                height='auto'
                playing={true}
                loop={true}
                muted={true}
                playbackRate={1}
                style={{
                    left: '0',
                    top: '0',
                    overflow: 'hidden',
                    zIndex:'-1',
                    maxHeight:'100vh',
                }}
            />
        </div>
    )
}

export default BackgroundPlayer
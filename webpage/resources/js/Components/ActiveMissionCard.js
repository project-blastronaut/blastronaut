import React from 'react';

export default function ActiveMissionCard({ mission, requirements, gathering }) {
    return (
        <div className="bg-white ">{console.log(mission)}

            <div className="max-w-2xl mx-auto py-8 px-4 grid items-center grid-cols-6 gap-y-16 gap-x-8 sm:px-6 sm:py-16 lg:max-w-7xl lg:px-8 mt-16">
                <div className="col-span-2">
                    <img
                        src={mission.image}
                        alt="Mission image"
                        className="bg-gray-100 rounded-lg"
                    />
                </div>
                <div className="col-span-4">
                    <h2 className="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">{mission.name}</h2>
                    <p className="mt-4 text-gray-500">
                        {mission.description}
                    </p>
                    <p className="mt-4 text-gray-500">
                        {"time: "+ mission.time_left + " minutes till the end"}
                    </p>
                    <dl className="mt-16 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 sm:gap-y-16 lg:gap-x-8">
                        {mission.resources.map((resource) => (
                            <div key={resource.id} className="border-t border-gray-200 pt-4">
                                <dt className="font-medium text-gray-900">{resource.name}</dt>
                                {resource.collected
                                ? <dd className="mt-2 text-sm text-gray-500">{resource.collected} / {resource.specified_amount}</dd>
                                : <dd className="mt-2 text-sm text-gray-500">0 / {resource.specified_amount} </dd>
                                }
                            </div>
                        ))}
                    </dl>
                </div>
               
            </div>
        </div>

    );
}

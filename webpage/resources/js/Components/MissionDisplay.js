import React, { useState } from 'react';
import { Inertia } from '@inertiajs/inertia';
import ResourceDisplay from '@/Components/ResourceDisplay';
import { useHistory } from "react-router-dom";
import MissionEditForm from '@/Components/MissionEditForm';

export default function MissionDisplay(props) {

    let colour;
    switch (props.faction) {
        case 3:
            colour = " bg-green-800 hover:bg-precorics"
            break;
        case 2:
            colour = " bg-purple-800 hover:bg-vector"
            break;
        case 1:
            colour = " bg-yellow-800 hover:bg-operative"
            break;
        default:
            break;
    }
    console.log(props);
    const repeatableText = props.repeatable==1 ? "Repeatable" : "One-time";

    const RemoveMission = () => {
        Inertia.delete(props.action + "/" + props.id)
    }

    const [showForm, setShowForm] = useState(false);
    const [showMission, setShowMission] = useState(true);

    const showFormFunc = () => {
        setShowForm(!showForm);
        setShowMission(!showMission);
    }

    const [showResources, setShowResources] = useState(false);

    const showResourcesFunc = () => {
        setShowResources(!showResources);
    }

    return (
        <>
            {showMission && (
                <div className={"flex flex-col items-start text-white" + colour}>
                    <div className={"flex flex-row w-full items-center space-x-4 text-white" + colour}>
                        <div className="flex flex-row flex-grow">
                            <img className="h-44 w-44 min-h-44 min-w-44" src={props.image}></img>
                        </div>
                        <div className="flex flex-col w-3/4">
                            <p className="text-3xl">{props.name}</p>
                            <p className="text-xl">{props.description}</p>
                            <p className="text-xl">{props.state + " " + repeatableText}</p>
                            <div className="grid grid-cols-3 gap-4">
                                {props.resources.map((item) => (
                                    <div className="" id={item.id}>
                                        <ResourceDisplay
                                            resource={item}
                                        />
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="flex flex-row items-center space-x-4 px-5 mr-8">
                            <button className="flex items-center bg-yellow-600 border border-transparent rounded-md text-3xl h-15 w-15 py-2 px-2 hover:bg-yellow-400"
                                onClick={showFormFunc}>&#x270D;</button>
                            <button className="flex items-center bg-red-600 border border-transparent rounded-md text-3xl h-15 w-15 py-2 px-2 hover:bg-red-400" onClick={RemoveMission}>🗑️</button>
                        </div>
                    </div>
                    {/*<button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 w-44 min-w-44" onClick={showResourcesFunc}>Resources</button>
                    {showResources && (
                        <ul className="space-y-2 w-3/4" id="ResourceList">
                            {props.requirements.map((item) => (
                                <li className="items-center" key={item.id}>
                                    <div className="" id={item.resource_id}>
                                        <ResourceDisplay
                                            requirement={item}
                                            resources={props.resources}
                                        />
                                    </div>
                                </li>
                            ))}
                        </ul>
                            )}*/}
                </div>
            )}
            {showForm && (
                <MissionEditForm
                    action={"/gm/missions/" + props.id}
                    name={props.name}
                    description={props.description}
                    image={props.image}
                    faction={props.faction}
                    factions={props.factions}
                    resources={props.resources}
                    allResources={props.allResources}
                    repeatable={props.repeatable}
                    showFormFunc={showFormFunc}
                />
            )}
        </>
    );
}
export default function TopPlayers(props) {
    return ( 
        <div className="flex items-end my-16">
            {console.log(props)}           
                {props.topPlayers.map((player, index) => (
                <div key={player.id} className={"order-"+props.order[index]+ " mx-auto w-1/4"}>
                    <div className="mt-1">
                        <img className="h-24 w-24 rounded-full m-auto object-cover" src={player.image} referrerPolicy="no-referrer"></img>
                    </div>
                    {player.name}
                    <span className={"w-full py-" + props.bar_sizes[index] + " bg-" + props.colors[index]+ " inline-block text-5xl align-bottom"}>{props.titles[index]}</span>
                    {player.credo}
                </div>
            ))}
        </div>
    )
}
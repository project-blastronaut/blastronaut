import Button from '@mui/material/Button';
import Dropdown from './Dropdown';

const navigation = [
  { name: 'Home', href: '/gm', color: '#212E38', radius: 2 },
  { name: 'Missions', href: '/gm/missions', color: '#212E38', radius: 2 },
  { name: 'Factions', href: '/gm/factions', color: '#212E38', radius: 2 },
  { name: 'DevTools', href: '/gm/devtools', color: '#212E38', radius: 2 },
  { name: 'Presskit', href: '/gm/presskit', color: '#212E38', radius: 2 },
  { name: 'Steam', href: 'https://store.steampowered.com/app/1392650/', color: '#891E2B', radius: 15, class: 'fatText' },
]


function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function NavBar({ active, auth }) {
  return (
    <div className="fixed z-40 flex bg-main w-full bg-backestBack">
      {navigation.map((item) => (
        <Button key={item.name}  className={item.class} href={item.href} style={{ margin: '1% auto', padding: '0 3%', background: item.color, color: 'white' }}
          sx={{ borderRadius: item.radius }}>
          {item.name}
        </Button>

      ))}
      <div className=" py-3">
        
        <div className="space-y-1">
          <Dropdown>
            <Dropdown.Trigger>
              <span className="inline-flex rounded-md">
                <button
                  type="button"
                  className="inline-flex items-center px-3 py-4 sm:py-2 border border-transparent text-xs sm:text-sm leading-4 font-medium rounded-md text-white bg-main hover:text-gray-300 focus:outline-none transition ease-in-out duration-150"
                >
                  {auth.user.name}
                  <svg
                    className="ml-2 -mr-0.5 h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </button>
              </span>
            </Dropdown.Trigger>
            <Dropdown.Content>
              <Dropdown.Link href={route('logout')} method="post" as="button">
                Log Out
              </Dropdown.Link>
            </Dropdown.Content>
          </Dropdown>
        </div>
      </div>
    </div>


  )
}
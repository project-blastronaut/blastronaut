<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('factions')->delete();
        
        \DB::table('factions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Operative United',
                'description' => 'placeholder',
                'created_at' => '2021-10-22 14:27:57',
                'updated_at' => '2021-12-01 10:30:36',
                'logo' => NULL,
                'voting_time_minutes' => 60,
                'mission_time_minutes' => 100000,
                'token' => 'OTAyMjQwNDE5NTU4MDc2NDY3.YXbi3g.AAQNmke4fO3ZuVA6SGfG9SSZDGc',
                'playerbase_size' => 102,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Vector Accelerate',
                'description' => 'placeholder',
                'created_at' => '2021-10-22 14:31:00',
                'updated_at' => '2021-11-30 19:19:51',
                'logo' => NULL,
                'voting_time_minutes' => 12,
                'mission_time_minutes' => 100000,
                'token' => 'OTAyMjQwNDE5NTU4MDc2NDY3.YXbi3g.AAQNmke4fO3ZuVA6SGfG9SSZDGc',
                'playerbase_size' => 10,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Precorics',
                'description' => 'placeholder',
                'created_at' => '2021-10-22 14:31:14',
                'updated_at' => '2021-11-16 17:16:40',
                'logo' => NULL,
                'voting_time_minutes' => 60,
                'mission_time_minutes' => 100000,
                'token' => 'OTAyMjQwNDE5NTU4MDc2NDY3.YXbi3g.AAQNmke4fO3ZuVA6SGfG9SSZDGc',
                'playerbase_size' => 10,
            ),
        ));
        
        
    }
}
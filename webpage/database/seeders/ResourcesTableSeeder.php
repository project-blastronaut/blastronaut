<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ResourcesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('resources')->delete();
        
        \DB::table('resources')->insert(array (
            0 => 
            array (
                'id' => 11,
                'created_at' => '2021-11-19 14:31:01',
                'updated_at' => '2021-12-02 10:14:54',
                'name' => 'üks',
                'image' => 'resource_icons/6trfbthjjPsvaOe5fzoXAOaZwRnMORwki3v5wpNd.png',
                'value' => 123.3,
            ),
            1 => 
            array (
                'id' => 12,
                'created_at' => '2021-11-19 14:39:56',
                'updated_at' => '2021-12-02 10:15:12',
                'name' => 'kaks',
                'image' => 'resource_icons/ZL6JPw1AZqJ6fG4pqV8HI6jzw20J5821Kstl1fi4.png',
                'value' => 23.4,
            ),
            2 => 
            array (
                'id' => 13,
                'created_at' => '2021-12-05 19:48:00',
                'updated_at' => '2021-12-05 19:48:00',
                'name' => 'Weed',
                'image' => 'resource_icons/LMqkYIyav2gvgfuhZNj62eBilt7YxohYTSWFNltw.png',
                'value' => 1.0,
            ),
            3 => 
            array (
                'id' => 14,
                'created_at' => '2021-12-05 19:48:23',
                'updated_at' => '2021-12-05 19:48:23',
                'name' => 'Niero',
                'image' => 'resource_icons/nvPuQf8pPEPqjjy5llhqQxufbJ9I3LJhQmcOwHT3.png',
                'value' => 1.0,
            ),
        ));
        
        
    }
}
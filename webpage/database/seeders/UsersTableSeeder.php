<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 16,
                'name' => 'Ratsemaat',
                'email' => 'rullherman@gmail.com',
                'email_verified_at' => NULL,
                'password' => NULL,
                'remember_token' => NULL,
                'created_at' => '2021-11-29 16:09:44',
                'updated_at' => '2021-12-01 09:46:56',
                'is_gm' => 1,
                'faction_id' => NULL,
                'image' => NULL,
                'discord_id' => '626146001522262023',
                'tag' => 'Ratsemaat#0718',
            ),
        ));
        
        
    }
}
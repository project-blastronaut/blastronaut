<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniquenessConstraintToMissionResourceRequirement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mission_resource_requirement', function (Blueprint $table) {
            $table->unique(["mission_id", "resource_id"], 'unique_resource_for_mission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mission_resource_requirement', function (Blueprint $table) {
        });
    }
}

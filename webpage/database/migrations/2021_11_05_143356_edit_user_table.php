<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_gm')->default(false);
            $table->integer('discord_id')->unique();
            $table->integer('credo')->default(0);
            $table->foreignId('faction_id')
            ->nullable()
            ->references('id')->on('factions')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

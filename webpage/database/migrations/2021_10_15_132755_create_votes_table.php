<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("title");
            $table->bigInteger('faction_id')->unsigned(); 
            $table->index('faction_id');
            $table->foreign('faction_id')
                ->unsigned()
                ->references('id')->on('factions')
                ->onDelete('cascade');
            
            $table->bigInteger('winning_mission_id')->unsigned();
            $table->index('winning_mission_id');
            $table->foreign('winning_mission_id')
                ->nullable()
                ->references('id')->on('missions')
                ->onDelete('cascade');
            $table->integer("voting_time_minutes");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}

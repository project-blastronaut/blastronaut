<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartedMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('started_missions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            
            $table->bigInteger('mission_id')->unsigned(); 
            $table->index('mission_id');
            $table->foreign('mission_id')
            ->references('id')->on('missions')
            ->onDelete('cascade');

            $table->bigInteger('resource_gathering_id')->unsigned(); 
            $table->index('resource_gathering_id');
            $table->foreign('resource_gathering_id')
            ->references('id')->on('resource_gatherings')
            ->onDelete('cascade');

            $table->boolean("wonVoting")->nullable();
            $table->boolean("accomplished")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('completed_missions');
    }
}

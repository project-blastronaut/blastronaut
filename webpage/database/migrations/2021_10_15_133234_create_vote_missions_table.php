<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoteMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote_missions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('vote_id')->unsigned(); 
            $table->index('vote_id');
            $table->foreign('vote_id')
                ->unsigned()
                ->references('id')->on('votes')
                ->onDelete('cascade');
            $table->bigInteger('mission_id')->unsigned(); 
            $table->index('mission_id');
            $table->foreign('mission_id')
                ->unsigned()
                ->references('id')->on('missions')
                ->onDelete('cascade');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vote_missions');
    }
}

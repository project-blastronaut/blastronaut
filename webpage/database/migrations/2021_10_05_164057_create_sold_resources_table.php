<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoldResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sold_resources', function (Blueprint $table) {
            $table->id();
            $table->integer("quantity");
            $table->bigInteger('resource_id')->unsigned(); 
            $table->index('resource_id');
            $table->bigInteger('player_id')->unsigned(); 
            $table->index('player_id');
            $table->bigInteger('faction_id')->unsigned(); 
            $table->index('faction_id');

            $table->foreign('resource_id')
            ->unsigned()
            ->references('id')->on('resources')
            ->onDelete('cascade');
            $table->foreign('player_id')
            ->nullable()
            ->unsigned()
            ->references('id')->on('players')
            ->onDelete('cascade');
            $table->foreign('faction_id')
            ->unsigned()
            ->references('id')->on('factions')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sold_resources');
    }
}

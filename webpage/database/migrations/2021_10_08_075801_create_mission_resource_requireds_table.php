<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionResourceRequiredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_resource_requireds', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->bigInteger('resource_id')->unsigned(); 
            $table->index('resource_id');
            $table->foreign('resource_id')
            ->unsigned()
            ->references('id')->on('resources')
            ->onDelete('cascade');

            $table->bigInteger('resource_gathering_id')->unsigned(); 
            $table->index('resource_gathering_id');
            $table->foreign('resource_gathering_id')
            ->references('id')->on('resource_gatherings')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission_resource_requireds');
    }
}

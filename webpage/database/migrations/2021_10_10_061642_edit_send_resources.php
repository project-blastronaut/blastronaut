<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditSendResources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('sold_resources', function($table){
            $table->dropForeign(['resource_gathering_id']);
            $table->dropColumn('resource_gathering_id');

            $table->bigInteger('mission_id')->unsigned(); 
            $table->index('mission_id');
            $table->foreign('mission_id')
                ->unsigned()
                ->references('id')->on('missions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionLifecycleParsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_lifecycle_pars', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->boolean("automatic");
            $table->integer("f1_voting_time");
            $table->integer("f2_voting_time");
            $table->integer("f3_voting_time");
            $table->integer("f1_mission_time");
            $table->integer("f2_mission_time");
            $table->integer("f3_mission_time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission_lifecycle_pars');
    }
}

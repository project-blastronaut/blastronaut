<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignKeyinSoldResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sold_resources', function (Blueprint $table) {
            $table->foreign('mission_id')
            ->unsigned()
            ->references('id')->on('missions')
            ->onDelete('cascade');
            $table->dropForeign(['player_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditSoldResources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sold_resources', function($table){
            $table->dropForeign(['faction_id']);
            $table->dropColumn('faction_id');

            $table->bigInteger('resource_gathering_id')->unsigned(); 
            $table->index('resource_gathering_id');
            $table->foreign('resource_gathering_id')
            ->references('id')->on('resource_gatherings')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

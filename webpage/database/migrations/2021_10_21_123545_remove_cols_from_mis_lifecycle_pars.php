<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColsFromMisLifecyclePars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
      
        Schema::table('mission_lifecycle_pars', function (Blueprint $table) {
            $table->dropColumn("f2_voting_time");
            $table->dropColumn("f1_voting_time");
            $table->dropColumn("f3_voting_time");
            $table->dropColumn("f1_mission_time");
            $table->dropColumn("f2_mission_time");
            $table->dropColumn("f3_mission_time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mis_lifecycle_pars', function (Blueprint $table) {
            //
        });
    }
}

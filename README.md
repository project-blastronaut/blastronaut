# Blastronaut meta-game in Discord

**Blastronaut webpage: https://blastronaut.optimatica.eu/**

**Blastronaut Universe Pre-Alpha Discord server: https://discord.gg/RKkzmqKx**

## Webpage
To start the webpage on your environment you have to have Linux. On windows you can use wsl2.
First you have to install Docker.

**git pull.**

**cd webpage**

Then you will have to run the command “**composer install**”.

Once the composer has finished installing, there should be /vendor folder in the webpage directory.
Before moving on, acquire the .env file and put it in the webpage folder.

Next run all of the following commands.
* ./vendor/bin/sail up
* ./vendor/bin/sail npm install
* ./vendor/bin/sail php artisan migrate
* ./vendor/bin/sail npm run prod

If any issues arise(most likely there will be some issues), contact us on Slack or visit https://laravel.com/docs/8.x/installation

## Discord bots

### Installation

Discord bots are developed with JavaScript and require:
* Node.js v16.6.0 or higher
* npm (Node Package Manager)

There are 4 different subprojects for bots in the repo:
* BlastronautBot
* CompanySocialManagerBot
* CompanyVoteManagerBot
* TestingBot

Each one must be installed separately with

**npm install**

in the subproject folder.

### Tokens

Each Discord bot has a unique token that is required for logging in with the bot. In our Discord bot subprojects the tokens are stored in .env files in the root folder of the subproject (except TestingBot which has its token in the config file). These files are not present in this online repo, because the tokens should be kept secret. You should get the .env files directly from us and then you can put them in their respective subprojects.

### Starting the bots

BlastronautBot’s code and TestingBot’s code are both meant for only one actual bot, but CompanySocialManagerBot’s code and CompanyVoteManagerBot’s code can be used to start different bots, depending on the “factionNumber” and “token” in the .env file. Different faction numbers and tokens should be commented out and only one faction number and token should be left in the .env file. Then, the bot can be started with

**node .\index.js**

in the root folder of the bot. Depending on the faction number and token in the .env file, different bots will be started:
* factionNumber=1: Operative United bot
* factionNumber=2: VectorAccelerate bot
* factionNumber=3: Precorics bot
* factionNumber=4: server-wide bot (only in CompanySocialManagerBot project, CompanyVoteManagerBot does not have a server-wide type of the bot)

TestingBot should not be started with “**node .\index.js**” at all. This bot is only meant for testing, which can be done with

**npm run test**

in the TestingBot root folder

### Config files
Configuration parameters for bots are stored in config files (in the config folder of each bot type).

The texts for the repeating webhook conversations in the chat channels in Discord are stored in factionConversations config files in the CompanySocialManagerBot subproject. Bot conversations can be customized by changing the conversations objects in these files. For example, new conversation objects can be added, but they must have the same structure as the current ones.
